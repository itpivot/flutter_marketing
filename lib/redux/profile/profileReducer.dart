import 'package:flutter_marketing/redux/profile/profileAction.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

@immutable
class ProfileState extends Equatable {
  final Map<String, dynamic> profile;

  ProfileState(
      {
      // this.profile = const { 'email': 'mary@gmail.com', 'name': 'Mary Jane', 'role': 'admin'  }
      //this.profile = const { 'username': '', 'name': '', 'role': ''  }
      this.profile = const {
        'code': '',
        'firstname': '',
        'lastname': '',
        'telephone': '',
        'role': ''
      }});

  ProfileState copyWith({Map<String, dynamic> profile}) {
    return ProfileState(profile: profile ?? this.profile);
  }

  @override
  List<Object> get props => [profile];
}

profileReducer(ProfileState state, dynamic action) {
  if (action is GetProfileAction) {
    return state.copyWith(profile: action.profileState.profile);
  }
  return state;
}
