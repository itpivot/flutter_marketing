import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_marketing/widgets/BottomMenuBar.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert' as convert;
import 'package:http/http.dart' as http;

class SummaryClosejobsPage extends StatefulWidget {
  SummaryClosejobsPage({Key key}) : super(key: key);

  @override
  _SummaryClosejobsPageState createState() => _SummaryClosejobsPageState();
}

class _SummaryClosejobsPageState extends State<SummaryClosejobsPage> {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();

  Map<String, dynamic> order;
  SharedPreferences prefs;
  bool isLoading = true;
  Map<String, dynamic> dataOrder = {
    'remark': '',
    'status': '',
  };
  Map<String, dynamic> detailMileStart;
  Map<String, dynamic> detailMileEnd;

  _initPrefs() async {
    prefs = await SharedPreferences.getInstance();
  }

  _getData(String locationId) async {
    try {
      var tokenString = prefs.getString('token');
      var token = convert.jsonDecode(tokenString);
      var url =
          '${DotEnv().env['DOMAIN']}/order/messenger/location/detail/${locationId.toString()}';
      var response = await http.get(
        url,
        headers: {'Authorization': 'Bearer ${token['access_token']}'},
      );
      if (response.statusCode == 200) {
        final Map<String, dynamic> dataMilenumber =
            convert.jsonDecode(response.body.toString());
        final List<dynamic> dataList = dataMilenumber['data'];
        print(dataList);
        Map milesList;
        setState(() {
          dataList.forEach((miles) => milesList = miles);
          dataOrder['remark'] = milesList['remark'];
          dataOrder['status'] = milesList['status_name'];
          dataOrder['signature'] = milesList['signature'];
          if (milesList['miles']?.isNotEmpty ?? true) {
            dataList.forEach(
              (miles) => miles['miles']['start'].forEach(
                (start) => detailMileStart = start,
              ),
            );
            dataList.forEach(
              (miles) => miles['miles']['end'].forEach(
                (end) => detailMileEnd = end,
              ),
            );
          }

          isLoading = false;
        });
      } else {
        //error 400,500
        setState(() {
          isLoading = false;
        });

        Flushbar(
          title: 'ผิดพลาด',
          message: 'เกิดข้อผิดพลาดจากระบบ',
          backgroundColor: Colors.redAccent,
          icon: Icon(
            Icons.error,
            size: 28.0,
            color: Colors.white,
          ),
          duration: Duration(seconds: 1),
          leftBarIndicatorColor: Colors.red,
        )..show(this.context);
      }
    } catch (e) {
      setState(() {
        isLoading = false;
      });

      Flushbar(
        title: 'ผิดพลาด',
        message: '${e.toString()}',
        backgroundColor: Colors.redAccent,
        icon: Icon(
          Icons.error,
          size: 28.0,
          color: Colors.white,
        ),
        duration: Duration(seconds: 2),
        leftBarIndicatorColor: Colors.red,
      )..show(this.context);
    }
  }

  @override
  void initState() {
    super.initState();
    _initPrefs();
    Future.delayed(Duration.zero, () {
      _getData(order['location_id']);
    });
  }

  @override
  Widget build(BuildContext context) {
    order = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60.0),
        child: AppBar(
          title: Transform(
            transform: Matrix4.translationValues(-5.0, 10.0, 0.0),
            child: Text(
              'ปิดงาน',
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
                color: Color(0xFF808080),
              ),
            ),
          ),
          backgroundColor: Colors.white,
          elevation: 0.0,
          titleSpacing: 0.0,
          leading: Builder(
            builder: (context) => IconButton(
              padding: EdgeInsets.only(top: 25.0),
              icon: FaIcon(
                FontAwesomeIcons.chevronLeft,
                size: 24,
                color: Color(0xFF808080),
              ),
              onPressed: () {
                Navigator.pushNamed(context, 'summarystack/summarydetailwork',
                    arguments: <String, dynamic>{
                      'order_id': order['order_id'],
                    });
              },
            ),
          ),
        ),
      ),
      body: isLoading == true
          ? Center(
              child: CircularProgressIndicator(),
            )
          : SingleChildScrollView(
              padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: Column(
                children: <Widget>[
                  Divider(
                    height: 30,
                  ),
                  Padding(
                    padding: EdgeInsets.zero,
                    child: FormBuilder(
                      key: _fbKey,
                      initialValue: {
                        'milenumber_start':
                            detailMileStart['mile_number'] != null
                                ? detailMileStart['mile_number']
                                : '',
                        'milenumber_end': detailMileEnd['mile_number'] != null
                            ? detailMileEnd['mile_number']
                            : '',
                        'status': dataOrder['status'] != null
                            ? dataOrder['status']
                            : '',
                        'remark': dataOrder['remark'] != null
                            ? dataOrder['remark']
                            : '',
                        'signature': dataOrder['signature'] != null
                            ? dataOrder['signature']
                            : '',
                      },
                      child: Column(
                        children: <Widget>[
                          SizedBox(
                            height: 50,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'เลขไมล์ตั้งต้น',
                                  style: TextStyle(
                                    fontSize: 16,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                height: 50.0,
                                width: MediaQuery.of(context).size.width * 0.15,
                                padding: EdgeInsets.only(right: 10.0),
                                child: detailMileStart == null
                                    ? Image(
                                        image: AssetImage(
                                            'assets/images/no_image.png'),
                                        fit: BoxFit.contain,
                                      )
                                    : Image.network(
                                        '${DotEnv().env['DOMAIN']}${detailMileStart['picture']}'),
                              ),
                              Container(
                                height: 50.0,
                                width: MediaQuery.of(context).size.width * 0.6,
                                child: FormBuilderTextField(
                                  attribute: "milenumber_start",
                                  enableInteractiveSelection: false,
                                  maxLines: 1,
                                  keyboardType: TextInputType.number,
                                  decoration: InputDecoration(
                                    isDense: true,
                                    contentPadding:
                                        EdgeInsets.fromLTRB(5, 20, 0, 0),
                                    fillColor: Colors.white,
                                    filled: true,
                                    hintText: "กรอกเลขไมล์ตั้งต้น",
                                    suffixIcon: Icon(
                                      Icons.edit,
                                      size: 18,
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(3)),
                                      borderSide: BorderSide(
                                        width: 1,
                                        color: Color(0xFFE7E7E7),
                                      ),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(3)),
                                      borderSide: BorderSide(
                                        width: 1,
                                        color: Color(0xFFE7E7E7),
                                      ),
                                    ),
                                  ),
                                  onTap: () {
                                    FocusScope.of(context)
                                        .requestFocus(new FocusNode());
                                  },
                                ),
                              ),
                              Container(
                                height: 50.0,
                                width: MediaQuery.of(context).size.width * 0.10,
                                padding: EdgeInsets.only(right: 10.0),
                                child: IconButton(
                                  icon: Icon(Icons.photo_camera),
                                  iconSize: 35,
                                  color: Color(0xFF808080),
                                  onPressed: null,
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            // width: MediaQuery.of(context).size.width * 1,
                            padding: EdgeInsets.only(top: 20.0),
                            child: RaisedButton(
                              onPressed: null,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    'บันทึก',
                                    style: TextStyle(
                                      fontSize: 16,
                                      color: Colors.white,
                                    ),
                                  ),
                                ],
                              ),
                              padding: EdgeInsets.all(8),
                              color: Color(0xFF3BB54A),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(3),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 50,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'เลขไมล์สิ้นสุด',
                                  style: TextStyle(
                                    fontSize: 16,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                height: 50.0,
                                width: MediaQuery.of(context).size.width * 0.15,
                                padding: EdgeInsets.only(right: 10.0),
                                child: detailMileEnd == null
                                    ? Image(
                                        image: AssetImage(
                                            'assets/images/no_image.png'),
                                        fit: BoxFit.contain,
                                      )
                                    : Image.network(
                                        '${DotEnv().env['DOMAIN']}${detailMileEnd['picture']}'),
                              ),
                              Container(
                                height: 50.0,
                                width: MediaQuery.of(context).size.width * 0.6,
                                child: FormBuilderTextField(
                                  attribute: "milenumber_end",
                                  enableInteractiveSelection: false,
                                  maxLines: 1,
                                  keyboardType: TextInputType.number,
                                  decoration: InputDecoration(
                                    isDense: true,
                                    contentPadding:
                                        EdgeInsets.fromLTRB(5, 20, 0, 0),
                                    fillColor: Colors.white,
                                    filled: true,
                                    hintText: "กรอกเลขไมล์สิ้นสุด",
                                    suffixIcon: Icon(
                                      Icons.edit,
                                      size: 18,
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(3)),
                                      borderSide: BorderSide(
                                        width: 1,
                                        color: Color(0xFFE7E7E7),
                                      ),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(3)),
                                      borderSide: BorderSide(
                                        width: 1,
                                        color: Color(0xFFE7E7E7),
                                      ),
                                    ),
                                  ),
                                  onTap: () {
                                    FocusScope.of(context)
                                        .requestFocus(new FocusNode());
                                  },
                                ),
                              ),
                              Container(
                                height: 50.0,
                                width: MediaQuery.of(context).size.width * 0.10,
                                padding: EdgeInsets.only(right: 10.0),
                                child: IconButton(
                                  icon: Icon(Icons.photo_camera),
                                  iconSize: 35,
                                  color: Color(0xFF808080),
                                  onPressed: null,
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            // width: MediaQuery.of(context).size.width * 1,
                            padding: EdgeInsets.only(top: 20.0),
                            child: RaisedButton(
                              onPressed: null,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    'บันทึก',
                                    style: TextStyle(
                                      fontSize: 16,
                                      color: Colors.white,
                                    ),
                                  ),
                                ],
                              ),
                              padding: EdgeInsets.all(8),
                              color: Color(0xFF3BB54A),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(3),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 50,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'สถานะงาน',
                                  style: TextStyle(
                                    fontSize: 16,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            height: 50.0,
                            width: MediaQuery.of(context).size.width * 1,
                            child: FormBuilderTextField(
                              enableInteractiveSelection: false,
                              attribute: "status",
                              maxLines: 1,
                              keyboardType: TextInputType.text,
                              decoration: InputDecoration(
                                isDense: true,
                                contentPadding: EdgeInsets.all(5),
                                fillColor: Colors.white,
                                filled: true,
                                focusedBorder: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(3)),
                                  borderSide: BorderSide(
                                    width: 1,
                                    color: Color(0xFFE7E7E7),
                                  ),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(3)),
                                  borderSide: BorderSide(
                                    width: 1,
                                    color: Color(0xFFE7E7E7),
                                  ),
                                ),
                              ),
                              onTap: () {
                                FocusScope.of(context)
                                    .requestFocus(new FocusNode());
                              },
                            ),
                          ),
                          SizedBox(
                            height: 50,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'หมายเหตุ',
                                  style: TextStyle(
                                    fontSize: 16,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            height: 90.0,
                            width: MediaQuery.of(context).size.width * 1,
                            child: FormBuilderTextField(
                              attribute: "remark",
                              enableInteractiveSelection: false,
                              maxLines: null,
                              minLines: 2,
                              keyboardType: TextInputType.multiline,
                              decoration: InputDecoration(
                                isDense: true,
                                contentPadding: EdgeInsets.all(5),
                                fillColor: Colors.white,
                                filled: true,
                                focusedBorder: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(3)),
                                  borderSide: BorderSide(
                                    width: 1,
                                    color: Color(0xFFE7E7E7),
                                  ),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(3)),
                                  borderSide: BorderSide(
                                    width: 1,
                                    color: Color(0xFFE7E7E7),
                                  ),
                                ),
                              ),
                              onTap: () {
                                FocusScope.of(context)
                                    .requestFocus(new FocusNode());
                              },
                            ),
                          ),
                          SizedBox(
                            height: 50,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'ผู้รับลงชื่อ',
                                  style: TextStyle(
                                    fontSize: 16,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.all(20),
                            child: dataOrder['signature'] == null ||
                                    dataOrder['signature'] == ''
                                ? Text('ไม่มีลายเซ็น')
                                : Image.network(
                                    '${DotEnv().env['DOMAIN']}${dataOrder['signature']}'),
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.grey)),
                          ),
                          Padding(
                            // width: MediaQuery.of(context).size.width * 1,
                            padding: EdgeInsets.only(top: 10.0),
                            child: RaisedButton(
                              onPressed: null,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    'บันทึกสถานะงาน',
                                    style: TextStyle(
                                      fontSize: 16,
                                      color: Colors.white,
                                    ),
                                  ),
                                ],
                              ),
                              padding: EdgeInsets.all(8),
                              color: Color(0xFF007BFF),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(3),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
      bottomNavigationBar: BottomMenuBar(),
    );
  }
}
