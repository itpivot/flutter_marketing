import 'dart:io';
import 'dart:convert';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_marketing/widgets/BottomMenuBar.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert' as convert;
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart';
import 'package:async/async.dart';
import 'package:geolocator/geolocator.dart';
import 'package:signature/signature.dart';

class ClosejobsPage extends StatefulWidget {
  ClosejobsPage({Key key}) : super(key: key);

  @override
  _ClosejobsPageState createState() => _ClosejobsPageState();
}

class _ClosejobsPageState extends State<ClosejobsPage> {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();

  final SignatureController _controller = SignatureController(
    penStrokeWidth: 1,
    penColor: Colors.black,
    exportBackgroundColor: Colors.white,
  );

  Map<String, dynamic> order;
  SharedPreferences prefs;
  bool isLoading = true;
  String remark;
  String signature;
  Map<String, dynamic> detailMileStart;
  Map<String, dynamic> detailMileEnd;

  File _imageStart;
  File _imageEnd;
  final picker = ImagePicker();

  String dropdownValue = 'สำเร็จ';
  String holderStatusJob = '';
  int statusId = 3;
  List<String> status = [
    'สำเร็จ',
    'ไม่สำเร็จ',
  ];

  TextEditingController txtMileStart = TextEditingController();
  TextEditingController txtMileEnd = TextEditingController();
  bool isMileStartValidate = false;
  bool isMileEndValidate = false;
  bool isSaveStart = false;
  bool isSaveEnd = false;

  _initPrefs() async {
    prefs = await SharedPreferences.getInstance();
  }

  _getData(String locationId) async {
    String pointNumber;
    try {
      var tokenString = prefs.getString('token');
      var token = convert.jsonDecode(tokenString);
      var url = '${DotEnv().env['DOMAIN']}/order/messenger/location/detail/${locationId.toString()}';
      var response = await http.get(
        url,
        headers: {'Authorization': 'Bearer ${token['access_token']}'},
      );
      if (response.statusCode == 200) {
        final Map<String, dynamic> dataMilenumber = convert.jsonDecode(response.body.toString());
        final List<dynamic> dataList = dataMilenumber['data'];

        pointNumber.toString();
        //print(dataList);
        //print('-2-->' + pointNumber);
        Map milesList;
        setState(() {
          dataList.forEach((miles) => milesList = miles);
          remark = milesList['remark'];
          if (milesList['miles']?.isNotEmpty ?? true) {
            dataList.forEach(
              (miles) => miles['miles']['start'].forEach(
                (start) => detailMileStart = start,
              ),
            );
            dataList.forEach(
              (miles) => miles['miles']['end'].forEach(
                (end) => detailMileEnd = end,
              ),
            );
          }
          txtMileStart.text = detailMileStart != null
              ? detailMileStart['mile_number']
              : order['last_mile'] != ''
                  ? order['last_mile']
                  : '';
          txtMileEnd.text = detailMileEnd != null
              ? detailMileEnd['mile_number']
              : order['last_mile'] != ''
                  ? order['last_mile']
                  : '';

          isLoading = false;
          if (detailMileStart != null) {
            isSaveStart = true;
          }
          if (detailMileEnd != null) {
            isSaveEnd = true;
          }
        });
      } else {
        //error 400,500
        setState(() {
          isLoading = false;
        });

        Flushbar(
          title: 'ผิดพลาด',
          message: 'เกิดข้อผิดพลาดจากระบบ',
          backgroundColor: Colors.redAccent,
          icon: Icon(
            Icons.error,
            size: 28.0,
            color: Colors.white,
          ),
          duration: Duration(seconds: 1),
          leftBarIndicatorColor: Colors.red,
        )..show(this.context);
      }
    } catch (e) {
      setState(() {
        isLoading = false;
      });

      Flushbar(
        title: 'ผิดพลาด',
        message: e.toString(),
        backgroundColor: Colors.redAccent,
        icon: Icon(
          Icons.error,
          size: 28.0,
          color: Colors.white,
        ),
        duration: Duration(seconds: 2),
        leftBarIndicatorColor: Colors.red,
      )..show(this.context);
    }
  }

  Future<void> getImage(String typeSave) async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        if (typeSave == 'start') {
          _imageStart = File(pickedFile.path);
        } else {
          _imageEnd = File(pickedFile.path);
        }
      } else {
        Flushbar(
          title: 'ผิดพลาด',
          message: 'ไม่มีข้อมูลรูปภาพ',
          backgroundColor: Colors.redAccent,
          icon: Icon(
            Icons.error,
            size: 28.0,
            color: Colors.white,
          ),
          duration: Duration(seconds: 1),
          leftBarIndicatorColor: Colors.red,
        )..show(this.context);
      }
    });
  }

  void getDropDownItem() {
    setState(() {
      holderStatusJob = dropdownValue;
    });
  }

  bool validateTextField(String userInput, String typeSave) {
    if (userInput?.isEmpty ?? true) {
      setState(() {
        if (typeSave == 'start') {
          isMileStartValidate = true;
        } else {
          isMileEndValidate = true;
        }
      });
      return false;
    }
    setState(() {
      if (typeSave == 'start') {
        isMileStartValidate = false;
      } else {
        isMileEndValidate = false;
      }
    });
    return true;
  }

  _saveMiles(
      File imageFile, String orderID, String pointNumber, String locationId, String typeSave, Map<String, dynamic> valuse, String lastMile) async {
    setState(() {
      isLoading = true;
    });

    try {
      if (lastMile == "") {
        lastMile = '0';
      }
      if (typeSave == 'start' && (int.parse(lastMile) != int.parse('${valuse['milenumber_start']}'))) {
        if (imageFile == null) {
          throw ("กรุณาถ่ายรูปเลขไมล์เริ่มต้น");
        }
      } else if (typeSave == 'end' && (int.parse('${valuse['milenumber_start']}') != int.parse('${valuse['milenumber_end']}'))) {
        if (imageFile == null) {
          throw ("กรุณาถ่ายรูปเลขไมล์สิ้นสุด");
        }
      }

      if (typeSave == 'end' && (int.parse('${valuse['milenumber_end']}') < int.parse('${valuse['milenumber_start']}'))) {
        throw ("คุณกรอกเลขไมล์ไม่ถูกต้อง");
      }

      var tokenString = prefs.getString('token');
      var token = convert.jsonDecode(tokenString);
      var url = '${DotEnv().env['DOMAIN']}/order/messenger/upload';

      var uri = Uri.parse(url);
      Map<String, String> headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${token['access_token']}',
      };
      final geoposition = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);

      var request = new http.MultipartRequest('POST', uri);
      //add headers
      request.headers.addAll(headers);
      //adding params
      request.fields['point'] = pointNumber.toString();
      request.fields['location_id'] = locationId.toString();
      request.fields['order_id'] = orderID.toString();
      request.fields['type'] = typeSave.toString();
      if (typeSave == 'start') {
        request.fields['mile_number'] = '${valuse['milenumber_start']}';
      } else {
        request.fields['mile_number'] = '${valuse['milenumber_end']}';
      }
      request.fields['latitude'] = '${geoposition.latitude}';
      request.fields['longtitude'] = '${geoposition.longitude}';

      if (imageFile != null) {
        // open a bytestream
        var stream = new http.ByteStream(DelegatingStream(imageFile.openRead()));
        // get file length
        var length = await imageFile.length();
        // multipart that takes file
        var multipartFile = new http.MultipartFile('mile_image', stream, length, filename: basename(imageFile.path));
        // add file to multipart
        request.files.add(multipartFile);
      } else {
        request.fields['mile_image'] = '';
      }

      // send
      var response = await request.send();
      // listen for response
      print(response);
      response.stream.transform(utf8.decoder).listen((value) {
        print(value);
      });

      if (response.statusCode == 200) {
        setState(() {
          isLoading = false;
          if (typeSave == 'start') {
            isSaveStart = true;
          } else {
            isSaveEnd = true;
          }
        });
        Flushbar(
          title: 'สำเร็จ',
          message: 'บันทึกข้อมูลสำเร็จ',
          backgroundColor: Colors.greenAccent,
          icon: Icon(
            Icons.check_circle,
            size: 28.0,
            color: Colors.white,
          ),
          duration: Duration(seconds: 1),
          leftBarIndicatorColor: Colors.greenAccent,
        )..show(this.context);
      } else {
        throw ("เกิดข้อผิดพลาดจากระบบ");
      }
    } catch (e) {
      setState(() {
        isLoading = false;
      });
      Flushbar(
        title: 'ผิดพลาด',
        message: e.toString(),
        backgroundColor: Colors.redAccent,
        icon: Icon(
          Icons.error,
          size: 28.0,
          color: Colors.white,
        ),
        duration: Duration(seconds: 2),
        leftBarIndicatorColor: Colors.red,
      )..show(this.context);
    }
  }

  _saveCloseJob(String orderID, String locationId, Map<String, dynamic> valuse) async {
    setState(() {
      isLoading = true;
    });

    try {
      if (!_controller.isNotEmpty) {
        if (valuse['remark'] == '') {
          throw ("หากไม่มีลายเซ็น กรุณาระบุหมายเหตุ");
        }
      } else {
        _signatureSave(orderID, locationId, '${order['point']}');
        if (signature == '') {
          throw ("หากไม่มีลายเซ็น กรุณาระบุหมายเหตุ");
        }
      }

      if (isSaveStart == true && isSaveEnd == true) {
        if (holderStatusJob == 'ไม่สำเร็จ') {
          setState(() {
            statusId = 4;
          });
        }
        var tokenString = prefs.getString('token');
        var token = convert.jsonDecode(tokenString);
        var url = '${DotEnv().env['DOMAIN']}/order/messenger/closejob';
        var response = await http.post(
          url,
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ${token['access_token']}',
          },
          body: convert.jsonEncode({
            'status_id': statusId,
            'remark': valuse['remark'],
            'location_id': locationId,
            'order_id': orderID,
          }),
        );
        if (response.statusCode == 200) {
          Navigator.pushNamed(this.context, 'inboxstack/detailwork', arguments: <String, dynamic>{
            'order_id': orderID,
          });
        } else {
          final Map<String, dynamic> feedback = convert.jsonDecode(response.body);
          throw ('${feedback['error']['message']}');
        }
      } else {
        throw ("กรุณาบัททึกเลขไมล์ให้ครบถ้วน");
      }
    } catch (e) {
      setState(() {
        isLoading = false;
      });

      Flushbar(
        title: 'ผิดพลาด',
        message: e.toString(),
        backgroundColor: Colors.redAccent,
        icon: Icon(
          Icons.error,
          size: 28.0,
          color: Colors.white,
        ),
        duration: Duration(seconds: 2),
        leftBarIndicatorColor: Colors.red,
      )..show(this.context);
    }
  }

  @override
  void initState() {
    super.initState();
    _initPrefs();
    Future.delayed(Duration.zero, () {
      _getData(order['location_id']);
    });
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    txtMileStart.dispose();
    txtMileEnd.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    order = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60.0),
        child: AppBar(
          title: Transform(
            transform: Matrix4.translationValues(-5.0, 10.0, 0.0),
            child: Text(
              'ปิดงาน',
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
                color: Color(0xFF808080),
              ),
            ),
          ),
          backgroundColor: Colors.white,
          elevation: 0.0,
          titleSpacing: 0.0,
          leading: Builder(
            builder: (context) => IconButton(
              padding: EdgeInsets.only(top: 25.0),
              icon: FaIcon(
                FontAwesomeIcons.chevronLeft,
                size: 24,
                color: Color(0xFF808080),
              ),
              onPressed: () {
                Navigator.pushNamed(context, 'inboxstack/detailwork', arguments: <String, dynamic>{
                  'order_id': order['order_id'],
                });
              },
            ),
          ),
        ),
      ),
      body: isLoading == true
          ? Center(
              child: CircularProgressIndicator(),
            )
          : SingleChildScrollView(
              padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: Column(
                children: <Widget>[
                  Divider(
                    height: 30,
                  ),
                  Padding(
                    padding: EdgeInsets.zero,
                    child: FormBuilder(
                      key: _fbKey,
                      initialValue: {
                        'milenumber_start': '',
                        'milenumber_end': '',
                        'remark': remark != null ? remark : '',
                      },
                      child: Column(
                        children: <Widget>[
                          SizedBox(
                            height: 50,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'เลขไมล์ตั้งต้น',
                                  style: TextStyle(
                                    fontSize: 16,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                height: 50.0,
                                width: MediaQuery.of(context).size.width * 0.15,
                                padding: EdgeInsets.only(right: 10.0),
                                child: _imageStart != null
                                    ? Image.file(
                                        _imageStart,
                                        fit: BoxFit.cover,
                                        height: double.infinity,
                                        width: double.infinity,
                                      )
                                    : detailMileStart == null
                                        ? Image(
                                            image: AssetImage('assets/images/no_image.png'),
                                            fit: BoxFit.contain,
                                          )
                                        : Image.network('${DotEnv().env['DOMAIN']}${detailMileStart['picture']}'),
                              ),
                              Container(
                                height: 50.0,
                                width: MediaQuery.of(context).size.width * 0.6,
                                child: FormBuilderTextField(
                                  attribute: "milenumber_start",
                                  maxLines: 1,
                                  keyboardType: TextInputType.number,
                                  controller: txtMileStart,
                                  decoration: InputDecoration(
                                    isDense: true,
                                    contentPadding: EdgeInsets.fromLTRB(5, 20, 0, 0),
                                    fillColor: Colors.white,
                                    filled: true,
                                    hintText: "กรอกเลขไมล์ตั้งต้น",
                                    suffixIcon: Icon(
                                      Icons.edit,
                                      size: 18,
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(3)),
                                      borderSide: BorderSide(
                                        width: 1,
                                        color: Color(0xFFE7E7E7),
                                      ),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(3)),
                                      borderSide: BorderSide(
                                        width: 1,
                                        color: Color(0xFFE7E7E7),
                                      ),
                                    ),
                                    errorText: isMileStartValidate ? 'กรุณากรอกเลขไมล์ตั้งต้น' : null,
                                    errorStyle: TextStyle(
                                      fontSize: 12.0,
                                      height: 1.0,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                height: 50.0,
                                width: MediaQuery.of(context).size.width * 0.10,
                                padding: EdgeInsets.only(right: 10.0),
                                child: IconButton(
                                  icon: Icon(Icons.photo_camera),
                                  iconSize: 35,
                                  color: Color(0xFF808080),
                                  onPressed: () {
                                    getImage('start');
                                  },
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            // width: MediaQuery.of(context).size.width * 1,
                            padding: EdgeInsets.only(top: 20.0),
                            child: RaisedButton(
                              onPressed: () {
                                setState(() {
                                  validateTextField(txtMileStart.text, 'start');
                                });
                                if (_fbKey.currentState.saveAndValidate() && isMileStartValidate == false) {
                                  _saveMiles(_imageStart, '${order['order_id']}', '${order['point']}', '${order['location_id']}', 'start',
                                      _fbKey.currentState.value, '${order['last_mile']}');
                                }
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    'บันทึก',
                                    style: TextStyle(
                                      fontSize: 16,
                                      color: Colors.white,
                                    ),
                                  ),
                                ],
                              ),
                              padding: EdgeInsets.all(8),
                              color: Color(0xFF3BB54A),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(3),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 50,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'เลขไมล์สิ้นสุด',
                                  style: TextStyle(
                                    fontSize: 16,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                height: 50.0,
                                width: MediaQuery.of(context).size.width * 0.15,
                                padding: EdgeInsets.only(right: 10.0),
                                child: _imageEnd != null
                                    ? Image.file(
                                        _imageEnd,
                                        fit: BoxFit.cover,
                                        height: double.infinity,
                                        width: double.infinity,
                                      )
                                    : detailMileEnd == null
                                        ? Image(
                                            image: AssetImage('assets/images/no_image.png'),
                                            fit: BoxFit.contain,
                                          )
                                        : Image.network('${DotEnv().env['DOMAIN']}${detailMileEnd['picture']}'),
                              ),
                              Container(
                                height: 50.0,
                                width: MediaQuery.of(context).size.width * 0.6,
                                child: FormBuilderTextField(
                                  attribute: "milenumber_end",
                                  maxLines: 1,
                                  keyboardType: TextInputType.number,
                                  controller: txtMileEnd,
                                  decoration: InputDecoration(
                                    isDense: true,
                                    contentPadding: EdgeInsets.fromLTRB(5, 20, 0, 0),
                                    fillColor: Colors.white,
                                    filled: true,
                                    hintText: "กรอกเลขไมล์สิ้นสุด",
                                    suffixIcon: Icon(
                                      Icons.edit,
                                      size: 18,
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(3)),
                                      borderSide: BorderSide(
                                        width: 1,
                                        color: Color(0xFFE7E7E7),
                                      ),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(3)),
                                      borderSide: BorderSide(
                                        width: 1,
                                        color: Color(0xFFE7E7E7),
                                      ),
                                    ),
                                    errorText: isMileEndValidate ? 'กรุณากรอกเลขไมล์สิ้นสุด' : null,
                                    errorStyle: TextStyle(
                                      fontSize: 12.0,
                                      height: 1.0,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                height: 50.0,
                                width: MediaQuery.of(context).size.width * 0.10,
                                padding: EdgeInsets.only(right: 10.0),
                                child: IconButton(
                                  icon: Icon(Icons.photo_camera),
                                  iconSize: 35,
                                  color: Color(0xFF808080),
                                  onPressed: () {
                                    getImage('end');
                                  },
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            // width: MediaQuery.of(context).size.width * 1,
                            padding: EdgeInsets.only(top: 20.0),
                            child: RaisedButton(
                              onPressed: () {
                                setState(() {
                                  validateTextField(txtMileEnd.text, 'end');
                                });
                                if (_fbKey.currentState.saveAndValidate() && isMileEndValidate == false) {
                                  _saveMiles(_imageEnd, '${order['order_id']}', '${order['point']}', '${order['location_id']}', 'end',
                                      _fbKey.currentState.value, '${order['last_mile']}');
                                }
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    'บันทึก',
                                    style: TextStyle(
                                      fontSize: 16,
                                      color: Colors.white,
                                    ),
                                  ),
                                ],
                              ),
                              padding: EdgeInsets.all(8),
                              color: Color(0xFF3BB54A),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(3),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 50,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'สถานะงาน',
                                  style: TextStyle(
                                    fontSize: 16,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width * 1,
                            padding: EdgeInsets.zero,
                            decoration: BoxDecoration(
                              border: Border.all(
                                width: 1,
                                color: Color(0xFFE7E7E7),
                              ),
                              borderRadius: BorderRadius.circular(3),
                            ),
                            child: Row(
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                                  child: DropdownButton<String>(
                                    value: dropdownValue,
                                    iconSize: 24,
                                    elevation: 16,
                                    style: TextStyle(
                                      fontSize: 16,
                                      color: Color(0xFF3F3F40),
                                      fontFamily: 'Prompt',
                                    ),
                                    underline: Container(),
                                    onChanged: (String data) {
                                      setState(() {
                                        dropdownValue = data;
                                      });
                                    },
                                    items: status.map<DropdownMenuItem<String>>((String value) {
                                      return DropdownMenuItem<String>(
                                        value: value,
                                        child: SizedBox(
                                          width: MediaQuery.of(context).size.width * 0.81,
                                          child: Text(value),
                                        ),
                                      );
                                    }).toList(),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 50,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'หมายเหตุ',
                                  style: TextStyle(
                                    fontSize: 16,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            height: 90.0,
                            width: MediaQuery.of(context).size.width * 1,
                            child: FormBuilderTextField(
                              attribute: "remark",
                              maxLines: null,
                              minLines: 2,
                              keyboardType: TextInputType.multiline,
                              decoration: InputDecoration(
                                isDense: true,
                                contentPadding: EdgeInsets.all(5),
                                fillColor: Colors.white,
                                filled: true,
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(3)),
                                  borderSide: BorderSide(
                                    width: 1,
                                    color: Color(0xFFE7E7E7),
                                  ),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(3)),
                                  borderSide: BorderSide(
                                    width: 1,
                                    color: Color(0xFFE7E7E7),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 50,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'ผู้รับลงชื่อ',
                                  style: TextStyle(
                                    fontSize: 16,
                                  ),
                                ),
                              ],
                            ),
                          ),

                          //OK AND CLEAR BUTTONS

                          Container(
                            padding: EdgeInsets.all(20),
                            child: signature == null || signature == ''
                                ? //SIGNATURE CANVAS
                                Container(
                                    child: Signature(
                                      controller: _controller,
                                      height: 300,
                                      backgroundColor: Colors.white,
                                    ),
                                  )
                                : Image.network('${DotEnv().env['DOMAIN']}/$signature'),
                            decoration: BoxDecoration(border: Border.all(color: Colors.grey)),
                          ),
                          Container(
                            decoration: const BoxDecoration(color: Colors.white),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                //SHOW EXPORTED IMAGE IN NEW ROUTE
                                RaisedButton(
                                  onPressed: () async {
                                    setState(() {
                                      signature = '';
                                      _signatureSaveclick(
                                        '${order['order_id']}',
                                        '${order['location_id']}',
                                        '${order['point']}',
                                      );
                                    });
                                  },
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        'บันทึก',
                                        style: TextStyle(
                                          fontSize: 16,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ],
                                  ),
                                  padding: EdgeInsets.all(8),
                                  color: Color(0xFF3BB54A),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(3),
                                  ),
                                ),

                                RaisedButton(
                                  onPressed: () {
                                    setState(() => _controller.clear());
                                    signature = '';
                                  },
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        'เซ็นใหม่',
                                        style: TextStyle(
                                          fontSize: 16,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ],
                                  ),
                                  padding: EdgeInsets.all(8),
                                  color: Color(0xFFeeee),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(3),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            // width: MediaQuery.of(context).size.width * 1,
                            padding: EdgeInsets.only(top: 10.0),
                            child: RaisedButton(
                              onPressed: () {
                                getDropDownItem();
                                if (_fbKey.currentState.saveAndValidate()) {
                                  _saveCloseJob('${order['order_id']}', '${order['location_id']}', _fbKey.currentState.value);
                                }
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    'บันทึกสถานะงาน',
                                    style: TextStyle(
                                      fontSize: 16,
                                      color: Colors.white,
                                    ),
                                  ),
                                ],
                              ),
                              padding: EdgeInsets.all(8),
                              color: Color(0xFF007BFF),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(3),
                              ),
                            ),
                          ),
                          SizedBox(height: 50),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
      bottomNavigationBar: BottomMenuBar(),
    );
  }

  void _signatureSave(String orderID, String locationId, String pointNumber) async {
    var now = new DateTime.now();
    var tokenString = prefs.getString('token');
    var token = convert.jsonDecode(tokenString);
    if (_controller.isNotEmpty) {
      final bytes = await _controller.toPngBytes();
      final pngBytes = bytes.buffer.asUint8List();
      String bs64 = base64Encode(pngBytes);
      var url = '${DotEnv().env['DOMAIN']}/order/messenger/signature';
      var response = await http.post(url,
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ${token['access_token']}',
          },
          body: convert.jsonEncode({
            'picture': 'data:image/jpeg;base64,' + bs64,
            'signature': 'data:image/jpeg;base64,' + bs64,
            'location_id': locationId,
            'order_id': orderID,
            'point': pointNumber,
          }));

      if (response.statusCode == 200) {
        print(response.body);
        setState(() {
          final Map<String, dynamic> feedback = convert.jsonDecode(response.body);
          signature = '${feedback['signature']}?d$now';
          // print(response.body);
          print(signature);
        });
      } else {
        print(response.body);
      }
    } else {
      print('error  ไม่มี img');
    }
  }

  void _signatureSaveclick(String orderID, String locationId, String pointNumber) async {
    var now = new DateTime.now();
    var tokenString = prefs.getString('token');
    var token = convert.jsonDecode(tokenString);
    if (_controller.isNotEmpty) {
      final bytes = await _controller.toPngBytes();
      final pngBytes = bytes.buffer.asUint8List();
      String bs64 = base64Encode(pngBytes);
      var url = '${DotEnv().env['DOMAIN']}/order/messenger/signature';
      var response = await http.post(url,
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ${token['access_token']}',
          },
          body: convert.jsonEncode({
            'picture': 'data:image/jpeg;base64,' + bs64,
            'signature': 'data:image/jpeg;base64,' + bs64,
            'location_id': locationId,
            'order_id': orderID,
            'point': pointNumber,
          }));

      if (response.statusCode == 200) {
        Flushbar(
          title: 'สำเร็จ',
          message: 'บันทึกข้อมูลสำเร็จ',
          backgroundColor: Colors.greenAccent,
          icon: Icon(
            Icons.check_circle,
            size: 28.0,
            color: Colors.white,
          ),
          duration: Duration(seconds: 1),
          leftBarIndicatorColor: Colors.greenAccent,
        )..show(this.context);

        setState(() {
          final Map<String, dynamic> feedback = convert.jsonDecode(response.body);
          signature = '${feedback['signature']}?d$now';
          // print(response.body);
          print(signature);
        });
      } else {
        Flushbar(
          title: 'ผิดพลาด',
          message: 'เกิดข้อผิดพลาดจากระบบ{$response.status_code}',
          backgroundColor: Colors.redAccent,
          icon: Icon(
            Icons.error,
            size: 28.0,
            color: Colors.white,
          ),
          duration: Duration(seconds: 1),
          leftBarIndicatorColor: Colors.red,
        )..show(this.context);
        print(response.body);
      }
    } else {
      print('error  ไม่มี img');
    }
  }
}
