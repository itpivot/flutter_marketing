import 'dart:convert';
import 'dart:typed_data';
import 'dart:ui';

import 'dart:ui' as ui;
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
// import 'package:flutter_signature_pad/flutter_signature_pad.dart';

class SignaturePage extends StatefulWidget {
  SignaturePage({Key key}) : super(key: key);

  @override
  _SignaturePageState createState() => _SignaturePageState();
}

class _SignaturePageState extends State<SignaturePage> {
  List<Path> _paths = <Path>[];
  ByteData imgBytes;
  String _msg = '';
  bool _loading = false;

  // Future<ui.Image> get rendered {
  //   ui.PictureRecorder recorder = ui.PictureRecorder();
  //   Canvas canvas = Canvas(recorder);
  //   Signature painter = Signature(paths: _paths);
  //   var size = context.size;
  //   painter.paint(canvas, size);
  //   return recorder.endRecording().toImage(size.width.floor(), size.height.floor());
  // }

  @override
  void initState() {
    super.initState();
  }

  _showImage() {
    showModalBottomSheet<void>(
        context: context,
        builder: (BuildContext context) {
          return ListView(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: ButtonTheme(
                  child: RaisedButton(
                    onPressed: () async {
                      String b64image = base64.encode(imgBytes.buffer.asUint8List());
                    },
                    child: Icon(
                      Icons.check,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height / 3,
                width: MediaQuery.of(context).size.width / 3,
                child: imgBytes != null
                    ? Image.memory(
                        new Uint8List.view(imgBytes.buffer),
                      )
                    : new Container(
                        height: 0,
                      ),
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60.0),
        child: AppBar(
          title: Transform(
            transform: Matrix4.translationValues(-5.0, 10.0, 0.0),
            child: Text(
              'เซ็นรับเอกสาร',
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
                color: Color(0xFF808080),
              ),
            ),
          ),
          backgroundColor: Colors.white,
          elevation: 0.0,
          titleSpacing: 0.0,
          leading: Builder(
            builder: (context) => IconButton(
              padding: EdgeInsets.only(top: 25.0),
              icon: FaIcon(
                FontAwesomeIcons.chevronLeft,
                size: 24,
                color: Color(0xFF808080),
              ),
              onPressed: () {
                Navigator.of(context, rootNavigator: true).pushNamedAndRemoveUntil('/inboxstack', (Route<dynamic> route) => false);
              },
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
        child: Column(
          children: <Widget>[
            Divider(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                SizedBox(
                  height: 20,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Icon(
                        Icons.calendar_today,
                        size: 18,
                        color: Color(0xFF808080),
                      ),
                      Text(
                        ' : 25/02/2021',
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 30,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Text(
                        'U300229757',
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(top: 8),
              child: Row(
                children: <Widget>[
                  Icon(
                    Icons.account_box,
                    size: 18,
                    color: Color(0xFF808080),
                  ),
                  Text(' : นาย สันต์ธีร์ ไขตะคุ'),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 8),
              child: Row(
                children: <Widget>[
                  Icon(
                    Icons.call,
                    size: 18,
                    color: Color(0xFF808080),
                  ),
                  Text(' : 092xxxxxxx'),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 8),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Text('ตึกฟู้ดแลนด์ ซุปเปอร์มาเก็ต ชั้นที่ 10 สุขุมวิท ซ.16 คลองเตย คลองเตย กรุงเทพมหานคร 10110'),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 5),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Text('จำนวนพื้นที่วิ่งงาน : จุด'),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 5),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Text('หมายเหตุ : '),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 5),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Icon(
                    Icons.fiber_manual_record,
                    size: 28,
                    color: Color(0xFF007BFF),
                  ),
                  Text(
                    'งานสำเร็จ',
                    style: TextStyle(
                      color: Color(0xFF007BFF),
                    ),
                  ),
                ],
              ),
            ),
            GestureDetector(
              onPanStart: (DragStartDetails details) {
                setState(() {
                  RenderBox box = context.findRenderObject();
                  Offset _localPosition = box.globalToLocal(details.globalPosition);
                  _localPosition = _localPosition.translate(0.0, -(AppBar().preferredSize.height + 20));
                  Path path = new Path();
                  path.moveTo(_localPosition.dx, _localPosition.dy);
                  _paths = List.from(_paths)..add(new Path.from(path));
                });
              },
              onPanUpdate: (DragUpdateDetails details) {
                setState(() {
                  RenderBox box = context.findRenderObject();
                  Offset _localPosition = box.globalToLocal(details.globalPosition);
                  _localPosition = _localPosition.translate(0.0, -(AppBar().preferredSize.height + 20));
                  Path path = _paths.last;
                  path.lineTo(_localPosition.dx, _localPosition.dy);
                });
              },
            ),
          ],
        ),
      ),
    );
  }
}

class Signature extends CustomPainter {
  List<Path> paths = <Path>[];

  Signature({this.paths});

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = new Paint()
      ..color = Colors.black
      ..strokeCap = StrokeCap.round
      ..strokeWidth = 5.0
      ..style = PaintingStyle.stroke;

    for (Path p in paths) {
      canvas.drawPath(p, paint);
    }
  }

  @override
  bool shouldRepaint(Signature oldDelegate) => true;
}
