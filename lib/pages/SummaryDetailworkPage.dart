import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_marketing/widgets/BottomMenuBar.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert' as convert;
import 'package:http/http.dart' as http;

class SummaryDetailworkPage extends StatefulWidget {
  SummaryDetailworkPage({Key key}) : super(key: key);

  @override
  _SummaryDetailworkPageState createState() => _SummaryDetailworkPageState();
}

class _SummaryDetailworkPageState extends State<SummaryDetailworkPage> {
  SharedPreferences prefs;
  bool isLoading = true;
  Map<String, dynamic> order;
  List<dynamic> data = [];

  _initPrefs() async {
    prefs = await SharedPreferences.getInstance();
  }

  _getData(String id) async {
    try {
      var tokenString = prefs.getString('token');
      var token = convert.jsonDecode(tokenString);
      var url =
          '${DotEnv().env['DOMAIN']}/order/messenger/locations/${id.toString()}';
      var response = await http.get(
        url,
        headers: {'Authorization': 'Bearer ${token['access_token']}'},
      );
      if (response.statusCode == 200) {
        final Map<String, dynamic> locations =
            convert.jsonDecode(response.body);
        setState(() {
          data = locations['data'];
          isLoading = false;
        });
      } else {
        //error 400,500
        setState(() {
          isLoading = false;
        });
        throw ("เกิดข้อผิดพลาดจากระบบ");
      }
    } catch (e) {
      setState(() {
        isLoading = false;
      });

      Flushbar(
        title: 'ผิดพลาด',
        message: '${e.toString()}',
        backgroundColor: Colors.redAccent,
        icon: Icon(
          Icons.error,
          size: 28.0,
          color: Colors.white,
        ),
        duration: Duration(seconds: 3),
        leftBarIndicatorColor: Colors.red,
      )..show(context);
    }
  }

  @override
  void initState() {
    super.initState();
    _initPrefs();
    Future.delayed(Duration.zero, () {
      _getData(order['order_id']);
    });
  }

  @override
  Widget build(BuildContext context) {
    order = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60.0),
        child: AppBar(
          title: Transform(
            transform: Matrix4.translationValues(-5.0, 10.0, 0.0),
            child: Text(
              'จุดรับ - ส่งงาน',
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
                color: Color(0xFF808080),
              ),
            ),
          ),
          backgroundColor: Colors.white,
          elevation: 0.0,
          titleSpacing: 0.0,
          leading: Builder(
            builder: (context) => IconButton(
              padding: EdgeInsets.only(top: 25.0),
              icon: FaIcon(
                FontAwesomeIcons.chevronLeft,
                size: 24,
                color: Color(0xFF808080),
              ),
              onPressed: () {
                Navigator.of(context, rootNavigator: true)
                    .pushNamedAndRemoveUntil(
                        '/summarystack', (Route<dynamic> route) => false);
              },
            ),
          ),
        ),
      ),
      body: isLoading == true
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Container(
              padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: Column(
                children: <Widget>[
                  Divider(
                    height: 30,
                  ),
                  Expanded(
                    child: data.length < 1
                        ? Text('ยังไม่มีงาน')
                        : ListView.separated(
                            padding: EdgeInsets.zero,
                            itemBuilder: (BuildContext context, int index) {
                              return Card(
                                margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(0.0),
                                ),
                                elevation: 0.0,
                                child: InkWell(
                                  child: Column(
                                    children: <Widget>[
                                      Padding(
                                        padding: EdgeInsets.only(top: 5),
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              child:
                                                  Text('จุดที่ : ${index + 1}'),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 8),
                                        child: Row(
                                          children: <Widget>[
                                            Icon(
                                              Icons.account_box,
                                              size: 18,
                                              color: Color(0xFF808080),
                                            ),
                                            Text(
                                                ' : ${data[index]['receiver_name']}'),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 8),
                                        child: Row(
                                          children: <Widget>[
                                            Icon(
                                              Icons.call,
                                              size: 18,
                                              color: Color(0xFF808080),
                                            ),
                                            Text(
                                                ' : ${data[index]['telephone']}'),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 8),
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: Text(
                                                  '${data[index]['address']}'),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 8),
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: data[index]['barcode'] ==
                                                      null
                                                  ? Text(
                                                      'เลขที่บิล / ใบเสร็จ : ')
                                                  : Text(
                                                      'เลขที่บิล / ใบเสร็จ : ${data[index]['barcode']}'),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 8),
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: data[index]['price'] ==
                                                      null
                                                  ? Text('จำนวนเงิน : ')
                                                  : Text(
                                                      'จำนวนเงิน : ${data[index]['price']}'),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 5),
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: Text(
                                                  'ประเภทงาน : ${data[index]['type_name']}'),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 5),
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: data[index][
                                                          'detail_type_work'] ==
                                                      null
                                                  ? Text('รายละเอียดงาน : ')
                                                  : Text(
                                                      'รายละเอียดงาน : ${data[index]['detail_type_work']}'),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 5),
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: data[index]
                                                          ['order_remark'] ==
                                                      null
                                                  ? Text('หมายเหตุ : ')
                                                  : Text(
                                                      'หมายเหตุ : ${data[index]['order_remark']}'),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Padding(
                                            padding: EdgeInsets.only(top: 5),
                                            child: Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Icon(
                                                  Icons.fiber_manual_record,
                                                  size: 28,
                                                  color: data[index]
                                                              ['status_id'] ==
                                                          3
                                                      ? Color(0xFF3BB54A)
                                                      : data[index][
                                                                  'status_id'] ==
                                                              4
                                                          ? Color(0xFFFF0000)
                                                          : Color(0xFF007BFF),
                                                ),
                                                Text(
                                                  ' ${data[index]['status_name']}',
                                                  style: TextStyle(
                                                    color: data[index]
                                                                ['status_id'] ==
                                                            3
                                                        ? Color(0xFF3BB54A)
                                                        : data[index][
                                                                    'status_id'] ==
                                                                4
                                                            ? Color(0xFFFF0000)
                                                            : Color(0xFF007BFF),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Padding(
                                            padding: EdgeInsets.only(top: 5),
                                            child: Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.end,
                                              children: <Widget>[
                                                SizedBox(
                                                  // width: MediaQuery.of(context).size.width * 0.25,
                                                  width: 120,
                                                  child: RaisedButton(
                                                    onPressed: () {
                                                      Navigator.pushNamed(
                                                          context,
                                                          'summarystack/summaryclosejobs',
                                                          arguments: <String,
                                                              dynamic>{
                                                            'order_id': order[
                                                                'order_id'],
                                                            'location_id': data[
                                                                    index][
                                                                'work_order_location_id'],
                                                          });
                                                    },
                                                    child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      children: <Widget>[
                                                        Text(
                                                          'ตรวจเช็ค',
                                                          style: TextStyle(
                                                            fontSize: 14,
                                                            color: Colors.white,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    color: Color(0xFF007BFF),
                                                    shape:
                                                        RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        3)),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            },
                            separatorBuilder:
                                (BuildContext context, int index) => Divider(),
                            itemCount: data.length,
                          ),
                  ),
                ],
              ),
            ),
      bottomNavigationBar: BottomMenuBar(),
    );
  }
}
