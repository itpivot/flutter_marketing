import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_marketing/widgets/BottomMenuBar.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert' as convert;
import 'package:http/http.dart' as http;

class SummaryPage extends StatefulWidget {
  SummaryPage({Key key}) : super(key: key);

  @override
  _SummaryPageState createState() => _SummaryPageState();
}

class _SummaryPageState extends State<SummaryPage> {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();

  final now = new DateTime.now();
  SharedPreferences prefs;
  List<dynamic> data = [];
  bool isLoading = true;
  String dateNow = '';

  _initPrefs() async {
    prefs = await SharedPreferences.getInstance();
  }

  _getDateNow() {
    dateNow = DateFormat('dd/MM/yyyy').format(now);
  }

  _getData() async {
    try {
      var tokenString = prefs.getString('token');
      var token = convert.jsonDecode(tokenString);
      var url = '${DotEnv().env['DOMAIN']}/order/messenger/summary';
      var response = await http.get(
        url,
        headers: {'Authorization': 'Bearer ${token['access_token']}'},
      );
      if (response.statusCode == 200) {
        final Map<String, dynamic> orders = convert.jsonDecode(response.body);
        setState(() {
          data = orders['data'];
          isLoading = false;
        });
      } else {
        //error 400,500
        setState(() {
          isLoading = false;
        });
        throw ("เกิดข้อผิดพลาดจากระบบ");
      }
    } catch (e) {
      setState(() {
        isLoading = false;
      });

      Flushbar(
        title: 'ผิดพลาด',
        message: 'เกิดข้อผิดพลาด ${e.toString()}',
        backgroundColor: Colors.redAccent,
        icon: Icon(
          Icons.error,
          size: 28.0,
          color: Colors.white,
        ),
        duration: Duration(seconds: 3),
        leftBarIndicatorColor: Colors.red,
      )..show(context);
    }
  }

  _search(Map<String, dynamic> valuse) async {
    try {
      var tokenString = prefs.getString('token');
      var token = convert.jsonDecode(tokenString);
      var url = '${DotEnv().env['DOMAIN']}/order/messenger/summary/search';
      var response = await http.post(
        url,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ${token['access_token']}',
        },
        body: convert.jsonEncode(
          {'keyword': valuse['search']},
        ),
      );
      if (response.statusCode == 200) {
        final Map<String, dynamic> orders = convert.jsonDecode(response.body);
        setState(() {
          data = orders['data'];
          isLoading = false;
        });
      } else {
        //error 400,500
        setState(() {
          isLoading = false;
        });

        Flushbar(
          title: 'ผิดพลาด',
          message: 'เกิดข้อผิดพลาดจากระบบ',
          backgroundColor: Colors.redAccent,
          icon: Icon(
            Icons.error,
            size: 28.0,
            color: Colors.white,
          ),
          duration: Duration(seconds: 3),
          leftBarIndicatorColor: Colors.red,
        )..show(context);
      }
    } catch (e) {
      setState(() {
        isLoading = false;
      });

      Flushbar(
        title: 'ผิดพลาด',
        message: 'เกิดข้อผิดพลาด ${e.toString()}',
        backgroundColor: Colors.redAccent,
        icon: Icon(
          Icons.error,
          size: 28.0,
          color: Colors.white,
        ),
        duration: Duration(seconds: 3),
        leftBarIndicatorColor: Colors.red,
      )..show(context);
    }
  }

  @override
  void initState() {
    super.initState();
    _initPrefs();
    Future.delayed(Duration.zero, () {
      _getDateNow();
      _getData();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: isLoading == true
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Container(
              padding: EdgeInsets.fromLTRB(20, 55, 20, 0),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      SizedBox(
                        height: 20,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'วันที่ : ${dateNow.toString()}',
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 30,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Text(
                              'จำนวนงาน : ${data.length}',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 15),
                    child: FormBuilder(
                      key: _fbKey,
                      initialValue: {
                        'search': '',
                      },
                      child: Column(
                        children: <Widget>[
                          FormBuilderTextField(
                            attribute: "search",
                            maxLines: 1,
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                              isDense: true,
                              contentPadding: EdgeInsets.all(5),
                              fillColor: Colors.white,
                              filled: true,
                              suffixIcon: IconButton(
                                icon: Icon(Icons.search),
                                iconSize: 20,
                                onPressed: () {
                                  if (_fbKey.currentState.saveAndValidate()) {
                                    _search(_fbKey.currentState.value);
                                  }
                                },
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(3)),
                                borderSide: BorderSide(
                                  width: 1,
                                  color: Color(0xFFE7E7E7),
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(3)),
                                borderSide: BorderSide(
                                  width: 1,
                                  color: Color(0xFFE7E7E7),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Divider(
                    height: 30,
                  ),
                  Expanded(
                    child: data.length < 1
                        ? Text('ยังไม่มีงาน')
                        : ListView.separated(
                            padding: EdgeInsets.zero,
                            itemBuilder: (BuildContext context, int index) {
                              return Card(
                                // margin: EdgeInsets.zero,
                                margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(0.0),
                                ),
                                elevation: 0.0,
                                child: InkWell(
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          SizedBox(
                                            child: Row(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Icon(
                                                  Icons.calendar_today,
                                                  size: 18,
                                                  color: Color(0xFF808080),
                                                ),
                                                Text(' : ${data[index]['date_meet']}'),
                                              ],
                                            ),
                                          ),
                                          SizedBox(
                                            child: Row(
                                              crossAxisAlignment: CrossAxisAlignment.end,
                                              children: <Widget>[
                                                Text('${data[index]['barcode']}'),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 8),
                                        child: Row(
                                          children: <Widget>[
                                            Icon(
                                              Icons.account_box,
                                              size: 18,
                                              color: Color(0xFF808080),
                                            ),
                                            Text(' : ${data[index]['receiver_name']}'),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 8),
                                        child: Row(
                                          children: <Widget>[
                                            Icon(
                                              Icons.call,
                                              size: 18,
                                              color: Color(0xFF808080),
                                            ),
                                            Text(' : ${data[index]['telephone']}'),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 8),
                                        child: Row(
                                          children: <Widget>[
                                            // FaIcon(
                                            //   FontAwesomeIcons.home,
                                            //   size: 18,
                                            // ),
                                            Expanded(
                                              child: Text('${data[index]['address']}'),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 5),
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: Text('ประเภทงาน : ${data[index]['type_name']}'),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 5),
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: data[index]['order_remark'] == null
                                                  ? Text('หมายเหตุ : ')
                                                  : Text('หมายเหตุ : ${data[index]['order_remark']}'),
                                              // child: Text('หมายเหตุ : ${data[index]['order_remark']}'),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 5),
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: Text('จุดรับ-ส่งเอกสาร : ${data[index]['locations']} จุด'),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Padding(
                                            padding: EdgeInsets.only(top: 5),
                                            child: Row(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Icon(
                                                  Icons.fiber_manual_record,
                                                  size: 28,
                                                  color: data[index]['status_id'] == 3
                                                      ? Color(0xFF3BB54A)
                                                      : data[index]['status_id'] == 4
                                                          ? Color(0xFFFF0000)
                                                          : Color(0xFF007BFF),
                                                ),
                                                Text(
                                                  ' ${data[index]['status_name']}',
                                                  style: TextStyle(
                                                    color: data[index]['status_id'] == 3
                                                        ? Color(0xFF3BB54A)
                                                        : data[index]['status_id'] == 4
                                                            ? Color(0xFFFF0000)
                                                            : Color(0xFF007BFF),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Padding(
                                            padding: EdgeInsets.only(top: 5),
                                            child: Row(
                                              crossAxisAlignment: CrossAxisAlignment.end,
                                              children: <Widget>[
                                                SizedBox(
                                                  // width: MediaQuery.of(context).size.width * 0.25,
                                                  width: 105,
                                                  child: RaisedButton(
                                                    onPressed: () {
                                                      Navigator.pushNamed(context, 'summarystack/summarydetailwork', arguments: <String, dynamic>{
                                                        'order_id': data[index]['work_order_id'],
                                                      });
                                                    },
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                      children: <Widget>[
                                                        Text(
                                                          'รายละเอียด',
                                                          style: TextStyle(
                                                            fontSize: 14,
                                                            color: Colors.white,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    color: Color(0xFF007BFF),
                                                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(3)),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            },
                            separatorBuilder: (BuildContext context, int index) => Divider(),
                            itemCount: data.length,
                          ),
                  )
                  // Divider(
                  //   height: 30,
                  // ),
                  // SizedBox(
                  //   height: 100,
                  // ),
                ],
              )),
      bottomNavigationBar: BottomMenuBar(),
    );
  }
}
