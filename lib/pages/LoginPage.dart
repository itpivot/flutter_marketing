import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_marketing/redux/appReducer.dart';
import 'package:flutter_marketing/redux/profile/profileAction.dart';
import 'dart:convert' as convert;
import 'package:http/http.dart' as http;
import 'package:flushbar/flushbar.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  AutovalidateMode _autovalidate = AutovalidateMode.disabled;
  bool isLoading = false;
  SharedPreferences prefs;

  _initPrefs() async {
    prefs = await SharedPreferences.getInstance();
  }

  @override
  void initState() {
    super.initState();
    _initPrefs();
  }

  _login(Map<dynamic, dynamic> values) async {
    setState(() {
      isLoading = true;
    });

    var url = '${DotEnv().env['DOMAIN']}/auth/login';
    var response = await http.post(url,
        headers: {'Content-Type': 'application/json'},
        body: convert.jsonEncode({'username': "${values['username']}", 'password': "${values['password']}"}));

    if (response.statusCode == 200) {
      setState(() {
        isLoading = false;
      });
      // save token to preferences
      await prefs.setString('token', response.body);
      //get profile
      _getProfile();
      // // goto home pages
      //Navigator.pushNamedAndRemoveUntil( context, '/home', (Route<dynamic> route) => false);
      //Navigator.pushNamed(context, '/home');
    } else if (response.statusCode == 401) {
      var feedback = convert.jsonDecode(response.body);
      setState(() {
        isLoading = false;
      });
      Flushbar(
        // title: 'ไม่สามารถเข้าระบบ',
        // message: ' ${feedback['error']['status']} ${feedback['error']['message']}',
        title: '${feedback['status_code']}',
        message: 'เกิดข้อผิดพลาดจากระบบ ${feedback['message']}',
        backgroundColor: Colors.redAccent,
        icon: Icon(
          Icons.error,
          size: 28.0,
          color: Colors.white,
        ),
        duration: Duration(seconds: 3),
        leftBarIndicatorColor: Colors.red,
      )..show(context);
    } else {
      var feedback = convert.jsonDecode(response.body);
      setState(() {
        isLoading = false;
      });
      Flushbar(
        title: 'ไม่สามารถเข้าระบบ',
        message: 'เกิดข้อผิดพลาดจากระบบ ${feedback['message']}',

        //title: '${feedback['message']}',
        // message: 'เกิดข้อผิดพลาดจากระบบ ${feedback['status_code']}',
        backgroundColor: Colors.redAccent,
        icon: Icon(
          Icons.error,
          size: 28.0,
          color: Colors.white,
        ),
        duration: Duration(seconds: 3),
        leftBarIndicatorColor: Colors.red,
      )..show(context);
    }
  }

  Future<void> _getProfile() async {
    // get token from prefs
    var tokenString = prefs.getString('token');
    var token = convert.jsonDecode(tokenString);
    // print(token['access_token']);

    // http get profile
    var url = '${DotEnv().env['DOMAIN']}/user/profile';
    var response = await http.get(url, headers: {'Authorization': 'Bearer ${token['access_token']}'});
    if (response.statusCode == 200) {
      // save user profile to prefs
      var profile = convert.jsonDecode(response.body);
      await prefs.setString('profile', convert.jsonEncode(profile));

      //call action
      final store = StoreProvider.of<AppState>(context);
      store.dispatch(getProfileAction(profile));

      Future.delayed(const Duration(milliseconds: 400), () {
        Navigator.pushNamed(context, '/home');
      });
    } else {
      var feedback = convert.jsonDecode(response.body);
      Flushbar(
        title: 'ไม่สามารถ Get profile ${feedback['error']['status']} ',
        message: '${feedback['error']['message']} : กรุณา Login ใหม่',

        //title: '${feedback['message']}',
        // message: 'เกิดข้อผิดพลาดจากระบบ ${feedback['status_code']}',
        backgroundColor: Colors.redAccent,
        icon: Icon(
          Icons.error,
          size: 28.0,
          color: Colors.white,
        ),
        duration: Duration(seconds: 3),
        leftBarIndicatorColor: Colors.red,
      )..show(context);
      Future.delayed(const Duration(seconds: 1), () {
        Navigator.pushNamed(context, '/login');
        //print('profile');
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/loginbg.png'),
                fit: BoxFit.cover,
              ),
              gradient:
                  LinearGradient(begin: Alignment.topRight, end: Alignment.bottomLeft, colors: [Colors.green[200], Theme.of(context).primaryColor])),
          child: Center(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Image(
                    image: AssetImage('assets/images/logo.png'),
                    fit: BoxFit.cover,
                    height: 150,
                  ),
                  SizedBox(height: 40),
                  new Container(
                    margin: const EdgeInsets.symmetric(vertical: 4.0, horizontal: 16.0),
                    child: new InkWell(
                      child: FormBuilder(
                        key: _fbKey,
                        initialValue: {
                          'username': '',
                          'password': '',
                        },
                        autovalidateMode: _autovalidate,
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 30,
                              width: 400,
                            ),
                            SizedBox(
                              width: 300,
                              child: FormBuilderTextField(
                                attribute: "username",
                                maxLines: 1,
                                keyboardType: TextInputType.emailAddress,
                                decoration: InputDecoration(
                                  labelText: "Username",
                                  contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                                  labelStyle: TextStyle(color: Colors.black87),
                                  fillColor: Colors.white,
                                  filled: true,
                                  border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(10))),
                                  errorStyle: TextStyle(color: Colors.red),
                                  suffixIcon: Icon(Icons.account_circle_rounded),
                                ),
                                validators: [FormBuilderValidators.required(errorText: 'กรุณากรอก Username')],
                              ),
                            ),
                            SizedBox(height: 20),
                            SizedBox(
                              width: 300,
                              child: FormBuilderTextField(
                                attribute: "password",
                                maxLines: 1,
                                obscureText: true,
                                keyboardType: TextInputType.visiblePassword,
                                decoration: InputDecoration(
                                  labelText: "Password",
                                  contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                                  labelStyle: TextStyle(color: Colors.black87),
                                  fillColor: Colors.white,
                                  filled: true,
                                  border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(10))),
                                  errorStyle: TextStyle(color: Colors.red),
                                  suffixIcon: Icon(Icons.lock_rounded),
                                ),
                                validators: [
                                  FormBuilderValidators.required(errorText: 'กรุณาป้อนข้อมูลรหัสผ่านด้วย'),
                                  FormBuilderValidators.minLength(3, errorText: 'กรุณาป้อนข้อมูล 3 ตัวขึ้นไป')
                                ],
                              ),
                            ),
                            SizedBox(height: 20),
                            SizedBox(
                              width: 300,
                              child: RaisedButton(
                                onPressed: () {
                                  setState(() {
                                    isLoading = true;
                                  });

                                  if (_fbKey.currentState.saveAndValidate()) {
                                    _login(_fbKey.currentState.value);
                                  } else {
                                    isLoading = false;
                                    setState(() {
                                      _autovalidate = AutovalidateMode.always;
                                    });
                                  }
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    isLoading == true
                                        ? CircularProgressIndicator(
                                            valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                                          )
                                        : Text(
                                            'login',
                                            style: TextStyle(
                                              fontSize: 20,
                                              color: Colors.white,
                                            ),
                                          ),
                                  ],
                                ),
                                padding: EdgeInsets.all(10),
                                color: Colors.orange,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                          ],
                        ),
                      ),
                    ),
                    decoration: new BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(
                        const Radius.circular(10),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
