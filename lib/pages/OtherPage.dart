import 'package:flutter/material.dart';
import 'package:flutter_marketing/widgets/BottomMenuBar.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OtherPage extends StatefulWidget {
  OtherPage({Key key}) : super(key: key);

  @override
  _OtherPageState createState() => _OtherPageState();
}

class _OtherPageState extends State<OtherPage> {
  _logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove('token');
    await prefs.remove('profile');
    //post logout to server

    Navigator.of(context, rootNavigator: true)
        .pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        padding: EdgeInsets.fromLTRB(20, 60, 20, 80),
        child: Column(
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'อื่นๆ',
                  style: TextStyle(
                    fontSize: 28,
                    fontWeight: FontWeight.bold,
                    color: Color(0xFF808080),
                  ),
                ),
              ],
            ),
            Divider(
              height: 40,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                GestureDetector(
                  onTap: () {},
                  child: Row(
                    children: <Widget>[
                      SizedBox(
                        width: 15,
                      ),
                      FaIcon(
                        FontAwesomeIcons.fileInvoice,
                        size: 28,
                        color: Color(0xFF808080),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Text(
                        ' รายงาน',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                          color: Color(0xFF808080),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Divider(
              height: 40,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                GestureDetector(
                  onTap: () {},
                  child: Row(
                    children: <Widget>[
                      SizedBox(
                        width: 15,
                      ),
                      FaIcon(
                        FontAwesomeIcons.fileInvoice,
                        size: 28,
                        color: Color(0xFF808080),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Text(
                        ' สั่งงาน',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                          color: Color(0xFF808080),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Divider(
              height: 40,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    _logout();
                  },
                  child: Row(
                    children: <Widget>[
                      SizedBox(
                        width: 15,
                      ),
                      FaIcon(
                        FontAwesomeIcons.signOutAlt,
                        size: 28,
                        color: Color(0xFF808080),
                      ),
                      SizedBox(
                        width: 15,
                      ),
                      Text(
                        ' ออกจากระบบ',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                          color: Color(0xFF808080),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Divider(
              height: 40,
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomMenuBar(),
    );

    // Container(
    //   child: Column(
    //     children: <Widget>[
    //       Row(
    //         mainAxisAlignment: MainAxisAlignment.start,
    //         children: <Widget>[
    //           SizedBox(
    //             child: Row(
    //               crossAxisAlignment: CrossAxisAlignment.start,
    //               children: [
    //                 FaIcon(
    //                   FontAwesomeIcons.solidCalendarAlt,
    //                   size: 18,
    //                 ),
    //                 Text(' : 25/03/2020'),
    //               ],
    //             ),
    //           ),
    //         ],
    //       ),
    //     ],
    //   ),
    // );
  }
}
