import 'package:flutter/material.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_marketing/widgets/BottomMenuBar.dart';
import 'package:intl/intl.dart';
import 'dart:convert' as convert;
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:geolocator/geolocator.dart';

class CheckinPage extends StatefulWidget {
  CheckinPage({Key key}) : super(key: key);

  @override
  _CheckinPageState createState() => _CheckinPageState();
}

class _CheckinPageState extends State<CheckinPage> {
  final now = new DateTime.now();
  String timeNow = '';
  String latitude = '';
  String longitude = '';
  String btnText = '';
  String txtName = '';
  String urlAttendance = '';
  SharedPreferences prefs;
  bool isButtonDisabled = true;
  String txtRamark = '';
  String setbtntext = '';
  AssetImage _imageType;

  Map<String, dynamic> profile = {
    'firstname': '',
    'lastname': '',
  };

  _initPrefs() async {
    prefs = await SharedPreferences.getInstance();
  }

  _getTime() {
    timeNow = DateFormat('Hm').format(now); //09:00
  }

  _getAttendance() async {
    var tokenString = prefs.getString('token');
    var token = convert.jsonDecode(tokenString);
    var url = '${DotEnv().env['DOMAIN']}/attendance';
    var response = await http.get(
      url,
      headers: {
        'Authorization': 'Bearer ${token['access_token']}',
      },
    );

    if (response.statusCode == 200) {
      final Map<String, dynamic> checkinStatus =
          convert.jsonDecode(response.body);
      var setURL = '';
      if (checkinStatus['checkin_status'] == 'new') {
        setState(() {
          setURL = '${DotEnv().env['DOMAIN']}/attendance/checkin';
          setbtntext = 'CHECK IN';
          txtRamark =
              'ก่อนเริ่มปฏิบัติงานอย่าลืมถ่ายรูปเลขไมล์เพื่อประโยชน์ของตัวท่านตัวเอง';
          _imageType = AssetImage('assets/images/checkin.png');
        });
      } else if (checkinStatus['checkin_status'] == 'checkin') {
        setState(() {
          setURL = '${DotEnv().env['DOMAIN']}/attendance/checkout';
          setbtntext = 'CHECK OUT';
          _imageType = AssetImage('assets/images/checkout.png');
        });
      } else {
        setState(() {
          setbtntext = 'CHECK OUT';
          isButtonDisabled = false;
          _imageType = AssetImage('assets/images/checkout.png');
        });
      }
      setState(() {
        btnText = setbtntext;
        urlAttendance = setURL;
      });
    } else {
      final Map<String, dynamic> feedback = convert.jsonDecode(response.body);
      Flushbar(
        title: '${feedback['error']['message']}',
        message: "เกิดข้อผิดพลาด ${feedback['error']['status']}",
        backgroundColor: Colors.redAccent,
        icon: Icon(
          Icons.error,
          size: 28.0,
          color: Colors.white,
        ),
        duration: Duration(seconds: 3),
        leftBarIndicatorColor: Colors.redAccent,
      )..show(context);
    }
  }

  _saveAttendance() async {
    if (urlAttendance != '') {
      final geoposition = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high);
      var tokenString = prefs.getString('token');
      var token = convert.jsonDecode(tokenString);
      var response = await http.post(
        urlAttendance,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ${token['access_token']}',
        },
        body: convert.jsonEncode({
          'latitude': geoposition.latitude,
          'longtitude': geoposition.longitude,
        }),
      );
      if (response.statusCode == 200) {
        Navigator.pushNamedAndRemoveUntil(
            context, '/home', (Route<dynamic> route) => false);
      } else if (response.statusCode == 401) {
        Navigator.of(context, rootNavigator: true).pushNamedAndRemoveUntil(
            '/logout', (Route<dynamic> route) => false);
      } else {
        final Map<String, dynamic> feedback = convert.jsonDecode(response.body);
        Flushbar(
          title: '${feedback['error']['message']}',
          message: "เกิดข้อผิดพลาด ${feedback['error']['status']}",
          backgroundColor: Colors.redAccent,
          icon: Icon(
            Icons.error,
            size: 28.0,
            color: Colors.white,
          ),
          duration: Duration(seconds: 3),
          leftBarIndicatorColor: Colors.redAccent,
        )..show(context);
      }
    }
  }

  _getProfile() async {
    var profileString = prefs.getString('profile');
    if (profileString != null) {
      setState(() {
        final Map<String, dynamic> profileData =
            convert.jsonDecode(profileString);
        profile = profileData['data'];
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _initPrefs();
    Future.delayed(Duration.zero, () {
      _getTime();
      _getAttendance();
      _getProfile();
    });
  }

  GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        padding: const EdgeInsets.fromLTRB(0, 70, 0, 70),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SafeArea(
              child: Column(
                children: <Widget>[
                  Text(
                    '${profile['firstname']} ${profile['lastname']}',
                    style: TextStyle(
                      fontSize: 30,
                      color: Color(0xFF333333),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    timeNow.toString(),
                    style: TextStyle(
                      fontSize: 65,
                      fontWeight: FontWeight.bold,
                      color: Color(0xFF333333),
                    ),
                  ),
                  SizedBox(
                    height: 50,
                  ),
                  Image(
                    image: _imageType == null
                        ? AssetImage('assets/images/loading.gif')
                        : _imageType,
                    fit: BoxFit.contain,
                    width: MediaQuery.of(context).size.width * 0.7,
                    height: MediaQuery.of(context).size.height * 0.3,
                  ),
                  SizedBox(
                    height: 45,
                    child: Row(
                      children: <Widget>[
                        Text(
                          txtRamark.toString(),
                          style: TextStyle(
                            fontSize: 11,
                            color: Color(0xFF333333),
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.7,
                    child: RaisedButton(
                      onPressed: isButtonDisabled == false
                          ? null
                          : () {
                              _saveAttendance();
                            },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            btnText.toString(),
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                      padding: EdgeInsets.all(10),
                      color: Color(0xFFFB872D),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomMenuBar(),
    );
  }
}
