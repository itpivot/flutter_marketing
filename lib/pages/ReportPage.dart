import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_marketing/widgets/BottomMenuBar.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_marketing/redux/appReducer.dart';
import 'package:flutter_marketing/redux/profile/profileAction.dart';
import 'package:flushbar/flushbar.dart';
import 'dart:convert' as convert;

import 'package:http/http.dart' as http;

class ReportPage extends StatefulWidget {
  ReportPage({Key key}) : super(key: key);

  @override
  _ReportPageState createState() => _ReportPageState();
}

class _ReportPageState extends State<ReportPage> {
  var about;

  SharedPreferences prefs;
  bool isLoading = true;
  bool pageNull = true;
  List<dynamic> datass = [];
  List<dynamic> datas = [];
  List<dynamic> infodata = [];
  Map<String, dynamic> totals;

  _getProfile() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var profileString = prefs.getString('profile');
    if (profileString != null) {
      var profile;
      setState(() {
        profile = convert.jsonDecode(profileString);
      });
      //call action
      final store = StoreProvider.of<AppState>(context);
      store.dispatch(getProfileAction(profile));
    }
  }

  _getreport() async {
    try {
      var url = '${DotEnv().env['DOMAIN']}/report';
      prefs = await SharedPreferences.getInstance();
      var tokenString = prefs.getString('token');
      var token = convert.jsonDecode(tokenString);
      var response = await http.get(url,
          headers: {'Authorization': 'Bearer ${token['access_token']}'});
      final Map<String, dynamic> feedback = convert.jsonDecode(response.body);
      if (response.statusCode == 200) {
        setState(() {
          isLoading = false;
          pageNull = false;
          // datass = data.data;
          datas = feedback['data'];
          infodata = feedback['info'];
          totals = feedback['total'];

          //datas.add(addtotal);
        });
        // print('inspect');
        //inspect(totals);
      } else if (response.statusCode == 401) {
        Navigator.of(context, rootNavigator: true).pushNamedAndRemoveUntil(
            '/logout', (Route<dynamic> route) => false);
      } else {
        setState(() {
          isLoading = false;
          pageNull = true;
        });
        print('error from backend ${response.statusCode}');
        Flushbar(
          title: ' ผิดพลาด ${response.statusCode}',
          message: 'เกิดข้อผิดพลาด ${feedback['error']['message']}',
          backgroundColor: Colors.redAccent,
          icon: Icon(
            Icons.error,
            size: 28.0,
            color: Colors.white,
          ),
          duration: Duration(seconds: 3),
          leftBarIndicatorColor: Colors.red,
        )..show(context);
      }
    } catch (e) {
      setState(() {
        isLoading = false;
        pageNull = true;
      });
      Flushbar(
        title: ' ผิดพลาด ',
        message: 'เกิดข้อผิดพลาด ${e.toString()}',
        backgroundColor: Colors.redAccent,
        icon: Icon(
          Icons.error,
          size: 28.0,
          color: Colors.white,
        ),
        duration: Duration(seconds: 3),
        leftBarIndicatorColor: Colors.red,
      )..show(context);
    }
  }

  @override
  void initState() {
    super.initState();
    _getreport();
    _getProfile();
  }

  @override
  Widget build(BuildContext context) {
    print('ibuid');
    var title = 'title';
    var subtitle = 'subtitle';
    if (infodata.length > 0) {
      title = infodata[0]['title'];
      subtitle = infodata[0]['sub_title'];
    }
    return isLoading == true
        ? Scaffold(
            body: Center(
              child: CircularProgressIndicator(),
            ),
            bottomNavigationBar: BottomMenuBar(),
          )
        : pageNull == true
            ? Scaffold(
                body: Center(
                  child: Text('ไม่พบข้อมูล',
                      style: TextStyle(
                        fontSize: 16,
                      )),
                ),
                bottomNavigationBar: BottomMenuBar(),
              )
            : Scaffold(
                body: ListView(
                  padding: const EdgeInsets.all(16),
                  children: [
                    SizedBox(height: 50),
                    SizedBox(
                      width: 300,
                      height: 30,
                      child: Center(
                        child: Text(title,
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            )),
                      ),
                    ),
                    SizedBox(
                      width: 300,
                      height: 30,
                      child: Center(
                        child: Text(subtitle,
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            )),
                      ),
                    ),
                    SizedBox(height: 20),
                    Padding(
                      padding: const EdgeInsets.all(8),
                      child: Table(
                        border: TableBorder(
                            horizontalInside: BorderSide(
                                width: 1,
                                color: Colors.grey,
                                style: BorderStyle.solid)),
                        children: [
                          TableRow(
                              decoration: BoxDecoration(
                                color: Colors.grey.shade300,
                              ),
                              children: [
                                TableCell(
                                    child: Container(
                                        padding: new EdgeInsets.all(10.0),
                                        child: Text('วันที่',
                                            style: TextStyle(
                                              fontSize: 18,
                                            )))),
                                TableCell(
                                    child: Container(
                                        padding: new EdgeInsets.all(10.0),
                                        child: Text('รวมจุด',
                                            style: TextStyle(
                                              fontSize: 18,
                                            )))),
                                TableCell(
                                    child: Container(
                                        padding: new EdgeInsets.all(10.0),
                                        child: Text('รวมไมล์',
                                            style: TextStyle(
                                              fontSize: 18,
                                            )))),
                                TableCell(
                                    child: Container(
                                        padding: new EdgeInsets.all(10.0),
                                        child: Text('ไมล์เกิน',
                                            style: TextStyle(
                                              fontSize: 18,
                                            )))),
                              ]),
                          for (var data in datas)
                            TableRow(children: [
                              TableCell(
                                  child: Container(
                                      padding: new EdgeInsets.all(10.0),
                                      child: Text('${data['date_closejob']}',
                                          style: TextStyle(
                                            fontSize: 15,
                                          )))),
                              TableCell(
                                  child: Container(
                                      padding: new EdgeInsets.all(10.0),
                                      child: Text('${data['sum_work_order']}',
                                          style: TextStyle(
                                            fontSize: 15,
                                          )))),
                              TableCell(
                                  child: Container(
                                      padding: new EdgeInsets.all(10.0),
                                      child: Text('${data['sum_mile']}',
                                          style: TextStyle(
                                            fontSize: 15,
                                          )))),
                              TableCell(
                                  child: Container(
                                      padding: new EdgeInsets.all(10.0),
                                      child: Text('${data['over_mile']}',
                                          style: TextStyle(
                                            fontSize: 15,
                                          )))),
                            ]),
                          TableRow(
                              decoration: BoxDecoration(
                                color: Colors.grey.shade300,
                              ),
                              children: [
                                TableCell(
                                    child: Container(
                                        padding: new EdgeInsets.all(10.0),
                                        child: Text('รวม',
                                            style: TextStyle(
                                              fontSize: 20,
                                            )))),
                                TableCell(
                                    child: Container(
                                        padding: new EdgeInsets.all(10.0),
                                        child: Text(
                                            '${totals['total_work_order']}',
                                            style: TextStyle(
                                              fontSize: 20,
                                            )))),
                                TableCell(
                                    child: Container(
                                        padding: new EdgeInsets.all(10.0),
                                        child: Text('${totals['total_mile']}',
                                            style: TextStyle(
                                              fontSize: 20,
                                            )))),
                                TableCell(
                                    child: Container(
                                        padding: new EdgeInsets.all(10.0),
                                        child:
                                            Text('${totals['total_over_mile']}',
                                                style: TextStyle(
                                                  fontSize: 20,
                                                )))),
                              ])
                        ],
                      ),
                    ),
                  ],
                ),
                bottomNavigationBar: BottomMenuBar(),
              );
  }
}
