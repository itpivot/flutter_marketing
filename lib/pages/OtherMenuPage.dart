import 'package:flutter/material.dart';
import 'package:flutter_marketing/widgets/BottomMenuBar.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_marketing/redux/appReducer.dart';
import 'package:flutter_marketing/redux/profile/profileAction.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'dart:convert' as convert;

class OtherMenuPage extends StatefulWidget {
  OtherMenuPage({Key key}) : super(key: key);

  @override
  _OtherMenuPageState createState() => _OtherMenuPageState();
}

class _OtherMenuPageState extends State<OtherMenuPage> {
  var about;
  _getProfile() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var profileString = prefs.getString('profile');
    if (profileString != null) {
      var profile;
      setState(() {
        profile = convert.jsonDecode(profileString);
      });
      //call action
      final store = StoreProvider.of<AppState>(context);
      store.dispatch(getProfileAction(profile));
    }
  }

  @override
  void initState() {
    super.initState();
    _getProfile();
  }

  Widget build(BuildContext context) {
    final List<String> img = <String>['other_icom02.png', 'other_icom01.png'];
    final List<String> txt = <String>['รายงาน', 'ออกจากระบบ'];
    final List<String> stacks = <String>['report', 'loout'];
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60.0),
        child: AppBar(
          title: Transform(
            transform: Matrix4.translationValues(-5.0, 10.0, 0.0),
            child: Text(
              'อื่น',
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
                color: Color(0xFF808080),
              ),
            ),
          ),
          backgroundColor: Colors.white,
          elevation: 0.0,
          titleSpacing: 0.0,
          leading: Builder(
            builder: (context) => IconButton(
              padding: EdgeInsets.only(top: 25.0),
              icon: FaIcon(
                FontAwesomeIcons.chevronLeft,
                size: 24,
                color: Color(0xFF808080),
              ),
              onPressed: () {
                Navigator.of(context, rootNavigator: true)
                    .pushNamedAndRemoveUntil(
                        '/inboxstack', (Route<dynamic> route) => false);
              },
            ),
          ),
        ),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Divider(),
            Expanded(
              child: ListView.separated(
                padding: const EdgeInsets.all(8),
                itemCount: img.length,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    decoration: BoxDecoration(
                      border: Border(
                        bottom: BorderSide(
                          color: Color(0xFFECECEC),
                        ),
                      ),
                    ),
                    child: ListTile(
                      leading: Container(
                        width: 80,
                        height: 25,
                        decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          image: DecorationImage(
                              image: AssetImage('assets/images/${img[index]}'),
                              fit: BoxFit.scaleDown),
                        ),
                      ),
                      title: Text(txt[index]),
                      onTap: () {
                        Navigator.pushNamed(
                            context, 'otherstack/${stacks[index]}');
                      },
                    ),
                  );
                },
                separatorBuilder: (BuildContext context, int index) =>
                    const SizedBox(),
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomMenuBar(),
    );
  }
}
