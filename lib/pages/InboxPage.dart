import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_marketing/widgets/BottomMenuBar.dart';
import 'dart:convert' as convert;
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import 'package:expandable_text/expandable_text.dart';

class InboxPage extends StatefulWidget {
  InboxPage({Key key}) : super(key: key);

  @override
  _InboxPageState createState() => _InboxPageState();
}

class _InboxPageState extends State<InboxPage> {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  final now = new DateTime.now();
  SharedPreferences prefs;
  List<dynamic> data = [];
  bool isLoading = true;
  String dateNow = '';

  _initPrefs() async {
    prefs = await SharedPreferences.getInstance();
  }

  _getDateNow() {
    dateNow = DateFormat('dd/MM/yyyy').format(now);
  }

  _getData() async {
    try {
      var tokenString = prefs.getString('token');
      var token = convert.jsonDecode(tokenString);
      var url = '${DotEnv().env['DOMAIN']}/order/messenger';
      var response = await http.get(
        url,
        headers: {'Authorization': 'Bearer ${token['access_token']}'},
      );
      if (response.statusCode == 200) {
        final Map<String, dynamic> orders = convert.jsonDecode(response.body);
        setState(() {
          data = orders['data'];
          isLoading = false;
        });
      } else {
        //error 400,500
        setState(() {
          isLoading = false;
        });
        throw ("เกิดข้อผิดพลาดจากระบบ");
      }
    } catch (e) {
      setState(() {
        isLoading = false;
      });

      Flushbar(
        title: 'ผิดพลาด',
        message: 'เกิดข้อผิดพลาด ${e.toString()}',
        backgroundColor: Colors.redAccent,
        icon: Icon(
          Icons.error,
          size: 28.0,
          color: Colors.white,
        ),
        duration: Duration(seconds: 3),
        leftBarIndicatorColor: Colors.red,
      )..show(context);
    }
  }

  _cancelOrder(String id) async {
    print(id);
    try {
      var tokenString = prefs.getString('token');
      var token = convert.jsonDecode(tokenString);
      var url = '${DotEnv().env['DOMAIN']}/order/messenger/return/${id.toString()}';
      var response = await http.post(
        url,
        headers: {'Authorization': 'Bearer ${token['access_token']}'},
      );
      if (response.statusCode == 200) {
        setState(() {
          isLoading = false;
        });
        Navigator.of(context).pushNamed('inboxstack/inbox').then((value) => setState(() {}));
      } else {
        //error 400,500
        setState(() {
          isLoading = false;
        });
        throw ("เกิดข้อผิดพลาดจากระบบ");
      }
    } catch (e) {
      setState(() {
        isLoading = false;
      });

      Flushbar(
        title: 'ผิดพลาด',
        message: 'เกิดข้อผิดพลาด ${e.toString()}',
        backgroundColor: Colors.redAccent,
        icon: Icon(
          Icons.error,
          size: 28.0,
          color: Colors.white,
        ),
        duration: Duration(seconds: 3),
        leftBarIndicatorColor: Colors.red,
      )..show(context);
    }
  }

  _search(Map<String, dynamic> valuse) async {
    try {
      var tokenString = prefs.getString('token');
      var token = convert.jsonDecode(tokenString);
      var url = '${DotEnv().env['DOMAIN']}/order/messenger/search';
      var response = await http.post(
        url,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ${token['access_token']}',
        },
        body: convert.jsonEncode(
          {'keyword': valuse['search']},
        ),
      );
      if (response.statusCode == 200) {
        final Map<String, dynamic> orders = convert.jsonDecode(response.body);
        setState(() {
          data = orders['data'];
          isLoading = false;
        });
      } else {
        //error 400,500
        setState(() {
          isLoading = false;
        });

        Flushbar(
          title: 'ผิดพลาด',
          message: 'เกิดข้อผิดพลาดจากระบบ',
          backgroundColor: Colors.redAccent,
          icon: Icon(
            Icons.error,
            size: 28.0,
            color: Colors.white,
          ),
          duration: Duration(seconds: 3),
          leftBarIndicatorColor: Colors.red,
        )..show(context);
      }
    } catch (e) {
      setState(() {
        isLoading = false;
      });

      Flushbar(
        title: 'ผิดพลาด',
        message: 'เกิดข้อผิดพลาด ${e.toString()}',
        backgroundColor: Colors.redAccent,
        icon: Icon(
          Icons.error,
          size: 28.0,
          color: Colors.white,
        ),
        duration: Duration(seconds: 3),
        leftBarIndicatorColor: Colors.red,
      )..show(context);
    }
  }

  @override
  void initState() {
    super.initState();
    _initPrefs();
    Future.delayed(Duration.zero, () {
      _getDateNow();
      _getData();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: isLoading == true
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Container(
              padding: EdgeInsets.fromLTRB(20, 55, 20, 0),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      SizedBox(
                        height: 20,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'วันที่ : ${dateNow.toString()}',
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 30,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Text(
                              'งานใหม่ : ${data.length}',
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 15),
                    child: FormBuilder(
                      key: _fbKey,
                      initialValue: {
                        'search': '',
                      },
                      child: Column(
                        children: <Widget>[
                          FormBuilderTextField(
                            attribute: "search",
                            maxLines: 1,
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                              isDense: true,
                              contentPadding: EdgeInsets.all(5),
                              fillColor: Colors.white,
                              filled: true,
                              suffixIcon: IconButton(
                                icon: Icon(Icons.search),
                                iconSize: 20,
                                onPressed: () {
                                  if (_fbKey.currentState.saveAndValidate()) {
                                    _search(_fbKey.currentState.value);
                                  }
                                },
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(3)),
                                borderSide: BorderSide(
                                  width: 1,
                                  color: Color(0xFFE7E7E7),
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(3)),
                                borderSide: BorderSide(
                                  width: 1,
                                  color: Color(0xFFE7E7E7),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Divider(
                    height: 30,
                  ),
                  Expanded(
                    child: data.length < 1
                        ? Text('ยังไม่มีงาน')
                        : ListView.separated(
                            padding: EdgeInsets.zero,
                            itemBuilder: (BuildContext context, int index) {
                              return Card(
                                // margin: EdgeInsets.zero,
                                margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(0.0),
                                ),
                                elevation: 0.0,
                                child: InkWell(
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          SizedBox(
                                            child: Row(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Icon(
                                                  Icons.calendar_today,
                                                  size: 18,
                                                  color: Color(0xFF808080),
                                                ),
                                                Text(' : ${data[index]['date_meet']}'),
                                              ],
                                            ),
                                          ),
                                          SizedBox(
                                            child: Row(
                                              crossAxisAlignment: CrossAxisAlignment.end,
                                              children: <Widget>[
                                                Text('${data[index]['barcode']}'),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 5),
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: Text(
                                                'ประเภท : ${data[index]['work_type']}',
                                                style: TextStyle(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.blue,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 0),
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: Text(
                                                'ผู้รับ :',
                                                style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 2),
                                        child: Row(
                                          children: <Widget>[
                                            // Icon(
                                            //   Icons.account_box,
                                            //   size: 18,
                                            //   color: Color(0xFF808080),
                                            // ),
                                            Expanded(
                                              child: Text('  ชื่อ : ${data[index]['receiver_name']}'),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 0),
                                        child: Row(
                                          children: <Widget>[
                                            // Icon(
                                            //   Icons.call,
                                            //   size: 18,
                                            //   color: Color(0xFF808080),
                                            // ),
                                            Text('  เบอร์โทร : ${data[index]['telephone']}'),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 0),
                                        child: Row(
                                          children: <Widget>[
                                            // FaIcon(
                                            //   FontAwesomeIcons.home,
                                            //   size: 18,
                                            // ),

                                            Expanded(
                                              child: Text('  ที่อยู่ : ${data[index]['address']}'),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 0),
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: data[index]['detail_type_work'] == null
                                                  ? Text('  ประเภทงาน : ${data[index]['type_name']}')
                                                  : Text('  ประเภทงาน : ${data[index]['type_name']} ( ${data[index]['detail_type_work']} )'),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 0),
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: data[index]['order_remark'] == null
                                                  ? Text(' หมายเหตุ : ')
                                                  : Text(
                                                      '  หมายเหตุ : ${data[index]['order_remark']}',
                                                      style: TextStyle(
                                                        color: Colors.red,
                                                      ),
                                                    ),
                                              // child: Text('หมายเหตุ : ${data[index]['order_remark']}'),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 0),
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: Text('  จุดรับ-ส่งเอกสาร : ${data[index]['locations']} จุด'),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 5),
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: Text(
                                                'ผู้ส่ง :',
                                                style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 0),
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              // child: ExpandableText(
                                              //   ' ชื่อ : ${data[index]['sender_name']} \n เบอร์โทร : ${data[index]['sender_telephone']}  \n ที่อยู่ : ${data[index]['sender_address']} ',
                                              //   expandText: 'แสดง',
                                              //   collapseText: 'ซ่อน',
                                              //   maxLines: 1,
                                              //   linkColor: Colors.blue,
                                              // ),
                                              child: Text(
                                                  ' ชื่อ : ${data[index]['sender_name']} \n เบอร์โทร : ${data[index]['sender_telephone']}  \n ที่อยู่ : ${data[index]['sender_address']} '),
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(height: 20.0),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Padding(
                                            padding: EdgeInsets.only(top: 5),
                                            child: Row(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Icon(
                                                  Icons.fiber_manual_record,
                                                  size: 28,
                                                  color: Color(0xFF007BFF),
                                                ),
                                                Text(
                                                  ' ${data[index]['status_name']}',
                                                  style: TextStyle(
                                                    color: Color(0xFF007BFF),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Padding(
                                            padding: EdgeInsets.only(top: 5),
                                            child: Row(
                                              crossAxisAlignment: CrossAxisAlignment.end,
                                              children: <Widget>[
                                                SizedBox(
                                                  // width: MediaQuery.of(context).size.width * 0.25,
                                                  width: 100,
                                                  child: RaisedButton(
                                                    onPressed: () {
                                                      _cancelOrder(data[index]['work_order_id']);
                                                    },
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                      children: <Widget>[
                                                        Text(
                                                          'ยกเลิก',
                                                          style: TextStyle(
                                                            fontSize: 14,
                                                            color: Colors.white,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    color: Color(0xFFC4C4C4),
                                                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(3)),
                                                  ),
                                                ),
                                                SizedBox(
                                                  // width: MediaQuery.of(context).size.width * 0.02,
                                                  width: 10,
                                                ),
                                                SizedBox(
                                                  // width: MediaQuery.of(context).size.width * 0.25,
                                                  width: 100,
                                                  child: RaisedButton(
                                                    onPressed: () {
                                                      Navigator.pushNamed(context, 'inboxstack/detailwork', arguments: <String, dynamic>{
                                                        'order_id': data[index]['work_order_id'],
                                                      });
                                                    },
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                      children: <Widget>[
                                                        Text(
                                                          'ส่งงาน',
                                                          style: TextStyle(
                                                            fontSize: 14,
                                                            color: Colors.white,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    color: Color(0xFF3BB54A),
                                                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(3)),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            },
                            separatorBuilder: (BuildContext context, int index) => Divider(),
                            itemCount: data.length,
                          ),
                  )
                  // Divider(
                  //   height: 30,
                  // ),
                  // SizedBox(
                  //   height: 100,
                  // ),
                ],
              )),
      bottomNavigationBar: BottomMenuBar(),
    );
  }
}
