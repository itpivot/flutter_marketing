import 'package:flutter/material.dart';
import 'package:flutter_marketing/redux/appReducer.dart';
import 'package:flutter_marketing/redux/profile/profileAction.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert' as convert;

class BottomMenuBar extends StatefulWidget {
  BottomMenuBar({Key key}) : super(key: key);

  @override
  _BottomMenuBarState createState() => _BottomMenuBarState();
}

class _BottomMenuBarState extends State<BottomMenuBar> {
  int _selectedItemIndex = 0;

  _getProfile() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var profileString = prefs.getString('profile');
    if (profileString != null) {
      var profile;
      setState(() {
        profile = convert.jsonDecode(profileString);
      });
      //call action
      final store = StoreProvider.of<AppState>(context);
      store.dispatch(getProfileAction(profile));
    }
  }

  @override
  void initState() {
    super.initState();
    _getProfile();
  }

  @override
  Widget build(BuildContext context) {
    if (ModalRoute.of(context).settings.name == '/home') {
      setState(() {
        _selectedItemIndex = 0;
      });
    } else if (ModalRoute.of(context).settings.name == 'inboxstack/inbox') {
      setState(() {
        _selectedItemIndex = 1;
      });
    } else if (ModalRoute.of(context).settings.name ==
        'inboxstack/detailwork') {
      setState(() {
        _selectedItemIndex = 1;
      });
    } else if (ModalRoute.of(context).settings.name == 'inboxstack/closejobs') {
      setState(() {
        _selectedItemIndex = 1;
      });
    } else if (ModalRoute.of(context).settings.name == 'summarystack/summary') {
      setState(() {
        _selectedItemIndex = 2;
      });
    } else if (ModalRoute.of(context).settings.name ==
        'summarystack/summarydetailwork') {
      setState(() {
        _selectedItemIndex = 2;
      });
    } else if (ModalRoute.of(context).settings.name ==
        'summarystack/summaryclosejobs') {
      setState(() {
        _selectedItemIndex = 2;
      });
    } else if (ModalRoute.of(context).settings.name == '/checkin') {
      setState(() {
        _selectedItemIndex = 3;
      });
    } else if (ModalRoute.of(context).settings.name == 'otherstack/other') {
      setState(() {
        _selectedItemIndex = 4;
      });
    } else if (ModalRoute.of(context).settings.name == 'otherstack/report') {
      setState(() {
        _selectedItemIndex = 4;
      });
    }

    return Row(
      children: <Widget>[
        buildNavBarItem(Icons.home, 'หน้าแรก', 0, '/home'),
        buildNavBarItem(Icons.inbox, 'งานของฉัน', 1, '/inboxstack'),
        buildNavBarItem(Icons.speed, 'สรุป', 2, '/summarystack'),
        buildNavBarItem(Icons.location_on, 'บันทึกเวลา', 3, '/checkin'),
        buildNavBarItem(Icons.format_align_justify, 'อื่นๆ', 4, '/otherstack'),
      ],
    );
  }

  Widget buildNavBarItem(
      IconData icon, String txtName, int index, String path) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context, rootNavigator: true).pushNamedAndRemoveUntil(
            path.toString(), (Route<dynamic> route) => false);
      },
      child: Container(
        padding: EdgeInsets.only(top: index == _selectedItemIndex ? 7 : 10),
        height: 60,
        width: MediaQuery.of(context).size.width / 5,
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border(
            top: BorderSide(
              color: index == _selectedItemIndex
                  ? Color(0xFFFB872D)
                  : Color(0xFFE7E7E7),
              width: index == _selectedItemIndex ? 3.0 : 0.7,
            ),
          ),
        ),
        child: Column(
          children: <Widget>[
            Icon(
              icon,
              color: index == _selectedItemIndex
                  ? Color(0xFFFB872D)
                  : Color(0xFF808080),
            ),
            Text(
              txtName,
              style: TextStyle(
                fontSize: 9,
                color: index == _selectedItemIndex
                    ? Color(0xFFFB872D)
                    : Color(0xFF808080),
              ),
            ),
          ],
        ),
        alignment: AlignmentDirectional(0.0, 0.0),
      ),
    );
  }
}
