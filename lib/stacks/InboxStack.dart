import 'package:flutter/material.dart';
import 'package:flutter_marketing/pages/ClosejobsPage.dart';
import 'package:flutter_marketing/pages/DetailworkPage.dart';
import 'package:flutter_marketing/pages/InboxPage.dart';
import 'package:flutter_marketing/pages/SignaturePage.dart';

class InboxStack extends StatefulWidget {
  InboxStack({Key key}) : super(key: key);

  @override
  _InboxStackState createState() => _InboxStackState();
}

class _InboxStackState extends State<InboxStack> {
  @override
  Widget build(BuildContext context) {
    return Navigator(
      initialRoute: 'inboxstack/inbox',
      onGenerateRoute: (RouteSettings settings) {
        WidgetBuilder builder;
        switch (settings.name) {
          case 'inboxstack/inbox':
            builder = (BuildContext _) => InboxPage();
            break;
          case 'inboxstack/detailwork':
            builder = (BuildContext _) => DetailworkPage();
            break;
          case 'inboxstack/closejobs':
            builder = (BuildContext _) => ClosejobsPage();
            break;
          case 'inboxstack/signature':
            builder = (BuildContext _) => SignaturePage();
            break;
          default:
            throw Exception('Invalid route: ${settings.name}');
        }
        return MaterialPageRoute(builder: builder, settings: settings);
      },
    );
  }
}
