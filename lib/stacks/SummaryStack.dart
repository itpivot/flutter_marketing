import 'package:flutter/material.dart';
import 'package:flutter_marketing/pages/SummaryClosejobsPage.dart';
import 'package:flutter_marketing/pages/SummaryDetailworkPage.dart';
import 'package:flutter_marketing/pages/SummaryPage.dart';

class SummaryStack extends StatefulWidget {
  SummaryStack({Key key}) : super(key: key);

  @override
  _SummaryStackState createState() => _SummaryStackState();
}

class _SummaryStackState extends State<SummaryStack> {
  @override
  Widget build(BuildContext context) {
    return Navigator(
      initialRoute: 'summarystack/summary',
      onGenerateRoute: (RouteSettings settings) {
        WidgetBuilder builder;
        switch (settings.name) {
          case 'summarystack/summary':
            builder = (BuildContext _) => SummaryPage();
            break;
          case 'summarystack/summarydetailwork':
            builder = (BuildContext _) => SummaryDetailworkPage();
            break;
          case 'summarystack/summaryclosejobs':
            builder = (BuildContext _) => SummaryClosejobsPage();
            break;
          default:
            throw Exception('Invalid route: ${settings.name}');
        }
        return MaterialPageRoute(builder: builder, settings: settings);
      },
    );
  }
}
