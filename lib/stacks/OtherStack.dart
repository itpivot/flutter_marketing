import 'package:flutter/material.dart';
import 'package:flutter_marketing/pages/OtherMenuPage.dart';
import 'package:flutter_marketing/pages/ReportPage.dart';
import 'package:flutter_marketing/pages/LogoutPage.dart';

class OtherStack extends StatefulWidget {
  OtherStack({Key key}) : super(key: key);

  @override
  _OtherStackState createState() => _OtherStackState();
}

class _OtherStackState extends State<OtherStack> {
  @override
  Widget build(BuildContext context) {
    return Navigator(
      initialRoute: 'otherstack/other',
      onGenerateRoute: (RouteSettings settings) {
        WidgetBuilder builder;
        switch (settings.name) {
          case 'otherstack/other':
            builder = (BuildContext _) => OtherMenuPage();
            break;
          case 'otherstack/report':
            builder = (BuildContext _) => ReportPage();
            break;
          case 'otherstack/loout':
            builder = (BuildContext _) => LogoutPage();
            break;
          default:
            throw Exception('Invalid route: ${settings.name}');
        }
        return MaterialPageRoute(builder: builder, settings: settings);
      },
    );
  }
}
