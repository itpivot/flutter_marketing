import 'package:flutter/material.dart';
import 'package:flutter_marketing/pages/CheckinPage.dart';
import 'package:flutter_marketing/pages/HomePage.dart';
import 'package:flutter_marketing/pages/LoginPage.dart';
import 'package:flutter_marketing/stacks/InboxStack.dart';
import 'package:flutter_marketing/stacks/SummaryStack.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_marketing/redux/appReducer.dart';

import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_marketing/stacks/OtherStack.dart';
import 'package:redux/redux.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

String token;
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  token = prefs.getString('token');

  final store = Store<AppState>(appReducer, initialState: AppState.initial());

  await DotEnv().load('.env');
  runApp(MyApp(store: store));
}

class MyApp extends StatelessWidget {
  final Store<AppState> store;
  MyApp({this.store});
  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
        title: 'Flutter Demo',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.grey,
          fontFamily: 'Prompt',
        ),
        // home: HomePage(),
        initialRoute: '/',
        routes: <String, WidgetBuilder>{
          '/': (context) => token == null ? LoginPage() : HomePage(),
          '/login': (context) => LoginPage(),
          '/checkin': (context) => CheckinPage(),
          '/home': (context) => HomePage(),
          '/inboxstack': (context) => InboxStack(),
          '/summarystack': (context) => SummaryStack(),
          '/otherstack': (context) => OtherStack(),
        },
      ),
    );
  }
}
